<!-- Detail Ukuran Produk Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('detail_ukuran_produk_id', 'Detail Ukuran Produk Id:') !!}
    {!! Form::select('detail_ukuran_produk_id', $detailUkuranProduks, null, ['class' => 'form-control custom-select']) !!}
</div>


<!-- Stock Field -->
<div class="form-group col-sm-6">
    {!! Form::label('stock', 'Stock:') !!}
    {!! Form::number('stock', null, ['class' => 'form-control']) !!}
</div>

<!-- Harga Beli Field -->
<div class="form-group col-sm-6">
    {!! Form::label('harga_beli', 'Harga Beli:') !!}
    {!! Form::number('harga_beli', null, ['class' => 'form-control']) !!}
</div>
