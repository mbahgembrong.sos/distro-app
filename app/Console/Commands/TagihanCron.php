<?php

namespace App\Console\Commands;

use App\Models\KeuanganKeluar;
use App\Models\Tagihanan;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class TagihanCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:tagihan';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $tagihans = Tagihanan::all();
        foreach ($tagihans as $tagihan) {
            if ($tagihan->jenis_tagihan == "Bulan") {
                $tagihanKeluar = KeuanganKeluar::where('foregin_id', $tagihan->id)->where('jenis', 'tagihan')->whereDate('created_at', '>=', env('SCEDJULE_TAHUN_BULAN', date('Y-m')) . '-01')->whereDate('created_at', '<=', env('SCEDJULE_TAHUN_BULAN_TANGGAL', date('Y-m-d')))->first();
                if ($tagihan->scedjule == env('TAGIHAN_BULAN_TANGGAL', date('m-d')) && $tagihanKeluar == null) {
                    $keuanganKeluar = new KeuanganKeluar();
                    $keuanganKeluar->nama = "Tagihan " . $tagihan->nama . ' pada Bulan ' . date('M');
                    $keuanganKeluar->jenis = "tagihan";
                    $keuanganKeluar->total = $tagihan->jumlah;
                    $keuanganKeluar->foto = null;
                    $keuanganKeluar->keterangan = 'belum bayar';
                    $keuanganKeluar->foregin_id = $tagihan->id;
                    $keuanganKeluar->save();
                    Log::info('Tagihan Tanggal ' . date('m-d') . ' ' . $tagihan->nama . ' telah ditambahkan');
                }
            } else {
                $tagihanKeluar = KeuanganKeluar::where('foregin_id', $tagihan->id)->where('jenis', 'tagihan')->whereDate('created_at', '>=', env('SCEDJULE_TAHUN_BULAN_TANGGAL', date('Y-m-d')))->first();
                if ($tagihan->scedjule == env('TAGIHAN_BULAN', date('m')) && $tagihanKeluar == null) {
                    $keuanganKeluar = new KeuanganKeluar();
                    $keuanganKeluar->nama = "Tagihan " . $tagihan->nama . ' ' . date('Y');
                    $keuanganKeluar->jenis = "tagihan";
                    $keuanganKeluar->total = $tagihan->jumlah;
                    $keuanganKeluar->foto = null;
                    $keuanganKeluar->keterangan = 'belum bayar';
                    $keuanganKeluar->foregin_id = $tagihan->id;
                    $keuanganKeluar->save();
                    Log::info('Tagihan Tahun ' . date('m') . ' ' . $tagihan->nama . ' telah ditambahkan');
                }
            }
        }
        Log::info('Tidak ada tagihan yang ditambahkan');
        return 0;
    }
}
