<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTagihananRequest;
use App\Http\Requests\UpdateTagihananRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Tagihanan;
use Illuminate\Http\Request;
use Flash;
use Response;

class TagihananController extends AppBaseController
{
    /**
     * Display a listing of the Tagihanan.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Tagihanan $tagihanans */
        $tagihanans = Tagihanan::all();

        return view('tagihanans.index')
            ->with('tagihanans', $tagihanans);
    }

    /**
     * Show the form for creating a new Tagihanan.
     *
     * @return Response
     */
    public function create()
    {
        return view('tagihanans.create');
    }

    /**
     * Store a newly created Tagihanan in storage.
     *
     * @param CreateTagihananRequest $request
     *
     * @return Response
     */
    public function store(CreateTagihananRequest $request)
    {
        $input = $request->all();
        if ($request->jenis_tagihan != 'Bulan') {
            $input["scedjule"] = explode('-', $request->scedjule)[0];
        }

        /** @var Tagihanan $tagihanan */
        $tagihanan = Tagihanan::create($input);

        Flash::success('Tagihanan saved successfully.');

        return redirect(route('tagihanans.index'));
    }

    /**
     * Display the specified Tagihanan.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Tagihanan $tagihanan */
        $tagihanan = Tagihanan::find($id);

        if (empty($tagihanan)) {
            Flash::error('Tagihanan not found');

            return redirect(route('tagihanans.index'));
        }

        return view('tagihanans.show')->with('tagihanan', $tagihanan);
    }

    /**
     * Show the form for editing the specified Tagihanan.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Tagihanan $tagihanan */
        $tagihanan = Tagihanan::find($id);

        if (empty($tagihanan)) {
            Flash::error('Tagihanan not found');

            return redirect(route('tagihanans.index'));
        }

        return view('tagihanans.edit')->with('tagihanan', $tagihanan);
    }

    /**
     * Update the specified Tagihanan in storage.
     *
     * @param int $id
     * @param UpdateTagihananRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTagihananRequest $request)
    {
        /** @var Tagihanan $tagihanan */
        $tagihanan = Tagihanan::find($id);

        if (empty($tagihanan)) {
            Flash::error('Tagihanan not found');

            return redirect(route('tagihanans.index'));
        }
        if ($request->jenis_tagihan != 'Bulan') {
            $request->scedjule = explode('-', $request->scedjule)[0];
        }

        $tagihanan->fill($request->all());
        $tagihanan->save();

        Flash::success('Tagihanan updated successfully.');

        return redirect(route('tagihanans.index'));
    }

    /**
     * Remove the specified Tagihanan from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Tagihanan $tagihanan */
        $tagihanan = Tagihanan::find($id);

        if (empty($tagihanan)) {
            Flash::error('Tagihanan not found');

            return redirect(route('tagihanans.index'));
        }

        $tagihanan->delete();

        Flash::success('Tagihanan deleted successfully.');

        return redirect(route('tagihanans.index'));
    }
}
