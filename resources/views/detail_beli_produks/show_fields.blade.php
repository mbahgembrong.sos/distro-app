<!-- Detail Ukuran Produk Id Field -->
<div class="col-sm-12">
    {!! Form::label('detail_ukuran_produk_id', 'Detail Ukuran Produk Id:') !!}
    <p>{{ $detailBeliProduk->detail_ukuran_produk_id }}</p>
</div>

<!-- Stock Field -->
<div class="col-sm-12">
    {!! Form::label('stock', 'Stock:') !!}
    <p>{{ $detailBeliProduk->stock }}</p>
</div>

<!-- Harga Beli Field -->
<div class="col-sm-12">
    {!! Form::label('harga_beli', 'Harga Beli:') !!}
    <p>{{ $detailBeliProduk->harga_beli }}</p>
</div>

