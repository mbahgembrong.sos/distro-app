<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class DetailTransaksi
 * @package App\Models
 * @version July 21, 2022, 2:42 pm UTC
 *
 */
class DetailTransaksi extends Model
{

    use HasFactory;

    public $table = 'detail_transaksis';




    public $fillable = ['detail_ukuran_produk_id', 'jumlah', 'total', 'transaksi_id'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'jumlah' => 'integer',
        'total' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [];
    /**
     * Get the transaksi that owns the DetailTransaksi
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function transaksi()
    {
        return $this->belongsTo(Transaksi::class, 'transaksi_id');
    }
    /**
     * Get the produk that owns the DetailTransaksi
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    /**
     * Get the detailUkuranProduk that owns the DetailTransaksi
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function detailUkuranProduk()
    {
        return $this->belongsTo(DetailUkuranProduk::class, 'detail_ukuran_produk_id', 'id');
    }
}
