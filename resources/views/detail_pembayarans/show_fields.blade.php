<!-- Transaksi Id Field -->
<div class="col-sm-12">
    {!! Form::label('transaksi_id', 'Transaksi Id:') !!}
    <p>{{ $detailPembayaran->transaksi_id }}</p>
</div>

<!-- Nama Field -->
<div class="col-sm-12">
    {!! Form::label('nama', 'Nama:') !!}
    <p>{{ $detailPembayaran->nama }}</p>
</div>

<!-- Bank Field -->
<div class="col-sm-12">
    {!! Form::label('bank', 'Bank:') !!}
    <p>{{ $detailPembayaran->bank }}</p>
</div>

<!-- Jumlah Field -->
<div class="col-sm-12">
    {!! Form::label('jumlah', 'Jumlah:') !!}
    <p>{{ $detailPembayaran->jumlah }}</p>
</div>

<!-- Tanggal Field -->
<div class="col-sm-12">
    {!! Form::label('tanggal', 'Tanggal:') !!}
    <p>{{ $detailPembayaran->tanggal }}</p>
</div>

<!-- Bukti Field -->
<div class="col-sm-12">
    {!! Form::label('bukti', 'Bukti:') !!}
    <p>{{ $detailPembayaran->bukti }}</p>
</div>

<!-- Validasi Field -->
<div class="col-sm-12">
    {!! Form::label('validasi', 'Validasi:') !!}
    <p>{{ $detailPembayaran->validasi }}</p>
</div>

