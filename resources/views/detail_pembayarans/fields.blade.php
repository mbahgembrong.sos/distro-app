<!-- Transaksi Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('transaksi_id', 'Transaksi Id:') !!}
    {!! Form::select('transaksi_id', $transaksis, null, ['class' => 'form-control custom-select']) !!}
</div>


<!-- Nama Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nama', 'Nama:') !!}
    {!! Form::text('nama', null, ['class' => 'form-control']) !!}
</div>

<!-- Bank Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bank', 'Bank:') !!}
    {!! Form::text('bank', null, ['class' => 'form-control']) !!}
</div>

<!-- Jumlah Field -->
<div class="form-group col-sm-6">
    {!! Form::label('jumlah', 'Jumlah:') !!}
    {!! Form::number('jumlah', null, ['class' => 'form-control']) !!}
</div>

<!-- Tanggal Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tanggal', 'Tanggal:') !!}
    {!! Form::text('tanggal', null, ['class' => 'form-control', 'id' => 'tanggal']) !!}
</div>

@push('page_scripts')
    <script type="text/javascript">
        $('#tanggal').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Bukti Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bukti', 'Bukti:') !!}
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('bukti', ['class' => 'custom-file-input']) !!}
            {!! Form::label('bukti', 'Choose file', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
</div>
<div class="clearfix"></div>
