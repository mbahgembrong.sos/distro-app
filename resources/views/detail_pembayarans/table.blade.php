<div class="table-responsive">
    <table class="table" id="detailPembayarans-table">
        <thead>
        <tr>
            <th>Transaksi Id</th>
        <th>Nama</th>
        <th>Bank</th>
        <th>Jumlah</th>
        <th>Tanggal</th>
        <th>Bukti</th>
        <th>Validasi</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($detailPembayarans as $detailPembayaran)
            <tr>
                <td>{{ $detailPembayaran->transaksi_id }}</td>
            <td>{{ $detailPembayaran->nama }}</td>
            <td>{{ $detailPembayaran->bank }}</td>
            <td>{{ $detailPembayaran->jumlah }}</td>
            <td>{{ $detailPembayaran->tanggal }}</td>
            <td>{{ $detailPembayaran->bukti }}</td>
            <td>{{ $detailPembayaran->validasi }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['detailPembayarans.destroy', $detailPembayaran->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('detailPembayarans.show', [$detailPembayaran->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('detailPembayarans.edit', [$detailPembayaran->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
