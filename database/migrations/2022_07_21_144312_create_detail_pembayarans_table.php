<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailPembayaransTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_pembayarans', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('transaksi_id');
            $table->string('nama');
            $table->string('bank');
            $table->integer('jumlah');
            $table->date('tanggal');
            $table->string('bukti');
            $table->boolean('validasi')->default(false);
            $table->timestamps();
        });
        Schema::table('detail_pembayarans', function (Blueprint  $table) {
            $table->foreign('transaksi_id')->references('id')->on('transaksis')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detail_pembayarans');
    }
}
