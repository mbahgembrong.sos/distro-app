<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PelangganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pelanggan = new \App\Models\Pelanggan();
        $pelanggan->nama = 'Pelanggan 1';
        $pelanggan->email = 'pelanggan@gmail.com';
        $pelanggan->password = bcrypt('pelanggan');
        $pelanggan->telp = '081234567890';
        $pelanggan->alamat = 'Jl. Kebon Jeruk No. 1';
        $pelanggan->save();
    }
}
