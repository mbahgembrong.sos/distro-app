<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTransaksiRequest;
use App\Http\Requests\UpdateTransaksiRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\DetailTransaksi;
use App\Models\DetailUkuranProduk;
use App\Models\Pelanggan;
use App\Models\Produk;
use App\Models\Transaksi;
use Illuminate\Http\Request;
use Flash;
use Response;

class TransaksiController extends AppBaseController
{
    /**
     * Display a listing of the Transaksi.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Transaksi $transaksis */
        $transaksis = Transaksi::all();

        return view('transaksis.index')
            ->with('transaksis', $transaksis);
    }

    /**
     * Show the form for creating a new Transaksi.
     *
     * @return Response
     */
    public function create()
    {
        $pelanggans = Pelanggan::pluck('nama', 'id');
        $produks = Produk::all();
        return view('transaksis.create', compact('pelanggans', 'produks'));
    }

    /**
     * Store a newly created Transaksi in storage.
     *
     * @param CreateTransaksiRequest $request
     *
     * @return Response
     */
    public function store(CreateTransaksiRequest $request)
    {

        // $input = $request->all();
        $transaksi = new Transaksi();
        $transaksi->pelanggan_id = $request->pelanggan_id;
        $transaksi->kasir_id = auth()->guard('karyawan')->user()->id;
        $transaksi->status = 'selesai';
        $transaksi->grand_total = 0;
        $transaksi->berat_total = 0;
        $transaksi->type_pembelian = "offline";
        $transaksi->save();
        $grand_total = 0;
        $berat_total = 0;
        foreach ($request->ukuran as $key => $item) {
            if ($item != null) {
                $ukuran = DetailUkuranProduk::find($item);
                $detailTransaksi = new DetailTransaksi();
                $detailTransaksi->transaksi_id = $transaksi->id;
                $detailTransaksi->detail_ukuran_produk_id = $item;
                $detailTransaksi->jumlah = $request->jumlah[$key];
                $detailTransaksi->total = $request->total[$key];
                $detailTransaksi->save();
                $grand_total += $request->total[$key];
                $berat_total += $ukuran->produk->berat * $request->jumlah[$key];
            }
        }
        $transaksi->grand_total = $grand_total;
        $transaksi->berat_total = $berat_total;
        $transaksi->save();


        Flash::success('Transaksi saved successfully.');

        return redirect(route('transaksis.index'));
    }

    /**
     * Display the specified Transaksi.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Transaksi $transaksi */
        $transaksi = Transaksi::find($id);

        if (empty($transaksi)) {
            Flash::error('Transaksi not found');

            return redirect(route('transaksis.index'));
        }

        return view('transaksis.show')->with('transaksi', $transaksi);
    }

    /**
     * Show the form for editing the specified Transaksi.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Transaksi $transaksi */
        $transaksi = Transaksi::find($id);

        if (empty($transaksi)) {
            Flash::error('Transaksi not found');

            return redirect(route('transaksis.index'));
        }
        $pelanggans = Pelanggan::pluck('nama', 'id');
        return view('transaksis.edit', compact('pelanggans'))->with('transaksi', $transaksi);
    }

    /**
     * Update the specified Transaksi in storage.
     *
     * @param int $id
     * @param UpdateTransaksiRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTransaksiRequest $request)
    {
        /** @var Transaksi $transaksi */
        $transaksi = Transaksi::find($id);

        if (empty($transaksi)) {
            Flash::error('Transaksi not found');

            return redirect(route('transaksis.index'));
        }

        $transaksi->fill($request->all());
        $transaksi->save();

        Flash::success('Transaksi updated successfully.');

        return redirect(route('transaksis.index'));
    }

    /**
     * Remove the specified Transaksi from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Transaksi $transaksi */
        $transaksi = Transaksi::find($id);

        if (empty($transaksi)) {
            Flash::error('Transaksi not found');

            return redirect(route('transaksis.index'));
        }

        $transaksi->delete();

        Flash::success('Transaksi deleted successfully.');

        return redirect(route('transaksis.index'));
    }
}
