@if (auth()->guard('karyawan')->user()->role()->first()->nama == 'Pemilik')
    <li class="nav-item">
        <a href="{{ route('roles.index') }}" class="nav-link {{ Request::is('roles*') ? 'active' : '' }}">
            <i class="fas fa-adjust"></i>
            <p>Roles</p>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('karyawans.index') }}" class="nav-link {{ Request::is('karyawans*') ? 'active' : '' }}">
            <i class="fas fa-adjust"></i>
            <p>Karyawans</p>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('produks.index') }}" class="nav-link {{ Request::is('produks*') ? 'active' : '' }}">
            <i class="fas fa-adjust"></i>
            <p>Produks</p>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('laporan.index') }}" class="nav-link {{ Request::is('laporan*') ? 'active' : '' }}">
            <i class="fas fa-adjust"></i>
            <p>Laporan</p>
        </a>
    </li>
@endif
@if (auth()->guard('karyawan')->user()->role()->first()->nama == 'Kasir')
    <li class="nav-item">
        <a href="{{ route('pelanggans.index') }}" class="nav-link {{ Request::is('pelanggans*') ? 'active' : '' }}">
            <i class="fas fa-adjust"></i>
            <p>Pelanggans</p>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('transaksis.index') }}" class="nav-link {{ Request::is('transaksis*') ? 'active' : '' }}">
            <i class="fas fa-adjust"></i>
            <p>Transaksis</p>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('detailTransaksis.index') }}"
            class="nav-link {{ Request::is('detailTransaksis*') ? 'active' : '' }}">
            <i class="fas fa-adjust"></i>
            <p>Detail Transaksis</p>
        </a>
    </li>


    <li class="nav-item">
        <a href="{{ route('detailPembayarans.index') }}"
            class="nav-link {{ Request::is('detailPembayarans*') ? 'active' : '' }}">
            <i class="fas fa-adjust"></i>
            <p>Detail Pembayarans</p>
        </a>
    </li>


    <li class="nav-item">
        <a href="{{ route('detailBeliProduks.index') }}"
            class="nav-link {{ Request::is('detailBeliProduks*') ? 'active' : '' }}">
            <i class="fas fa-adjust"></i>
            <p>Detail Beli Produks</p>
        </a>
    </li>
@endif
@if (auth()->guard('karyawan')->user()->role()->first()->nama == 'Keuangan')
    <li class="nav-item">
        <a href="{{ route('tagihanans.index') }}" class="nav-link {{ Request::is('tagihanans*') ? 'active' : '' }}">
            <i class="fas fa-adjust"></i>
            <p>Tagihanans</p>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('keuanganKeluars.index') }}"
            class="nav-link {{ Request::is('keuanganKeluars*') ? 'active' : '' }}">
            <i class="fas fa-adjust"></i>
            <p>Keuangan Keluars</p>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('jurnal_harian.keuangan_masuk') }}"
            class="nav-link {{ Request::is('jurnal_harian/keuangan_masuk') ? 'active' : '' }}">
            <i class="fas fa-adjust"></i>
            <p>Keuangan Masuk</p>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('jurnal_harian.index') }}"
            class="nav-link {{ Request::is('jurnal_harian') ? 'active' : '' }}">
            <i class="fas fa-adjust"></i>
            <p>Jurnal Harian</p>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('produks.index') }}" class="nav-link {{ Request::is('produks*') ? 'active' : '' }}">
            <i class="fas fa-adjust"></i>
            <p>Produks</p>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('laporan.index') }}" class="nav-link {{ Request::is('laporan*') ? 'active' : '' }}">
            <i class="fas fa-adjust"></i>
            <p>Laporan</p>
        </a>
    </li>
@endif
@if (auth()->guard('karyawan')->user()->role()->first()->nama == 'Admin Gudang')
    <li class="nav-item">
        <a href="{{ route('kategoris.index') }}" class="nav-link {{ Request::is('kategoris*') ? 'active' : '' }}">
            <i class="fas fa-adjust"></i>
            <p>Kategoris</p>
        </a>
    </li>

    <li class="nav-item">
        <a href="{{ route('produks.index') }}" class="nav-link {{ Request::is('produks*') ? 'active' : '' }}">
            <i class="fas fa-adjust"></i>
            <p>Produks</p>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('detailUkuranProduks.index') }}"
            class="nav-link {{ Request::is('detailUkuranProduks*') ? 'active' : '' }}">
            <i class="fas fa-adjust"></i>
            <p>Detail Ukuran Produks</p>
        </a>
    </li>
@endif
@if (auth()->guard('web')->check())
    <li class="nav-item">
        <a href="{{ route('keranjangs.index') }}" class="nav-link {{ Request::is('keranjangs*') ? 'active' : '' }}">
            <p>Keranjangs</p>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('transaksis.index') }}" class="nav-link {{ Request::is('transaksis*') ? 'active' : '' }}">
            <i class="fas fa-adjust"></i>
            <p>Transaksis</p>
        </a>
    </li>
@endif
