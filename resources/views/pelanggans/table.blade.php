<div class="table-responsive">
    <table class="table" id="pelanggans-table">
        <thead>
        <tr>
            <th>Nama</th>
        <th>Email</th>
        <th>Password</th>
        <th>Telp</th>
        <th>Alamat</th>
        <th>Kodepos</th>
        <th>Kota Id</th>
        <th>Foto</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($pelanggans as $pelanggan)
            <tr>
                <td>{{ $pelanggan->nama }}</td>
            <td>{{ $pelanggan->email }}</td>
            <td>{{ $pelanggan->password }}</td>
            <td>{{ $pelanggan->telp }}</td>
            <td>{{ $pelanggan->alamat }}</td>
            <td>{{ $pelanggan->kodepos }}</td>
            <td>{{ $pelanggan->kota_id }}</td>
            <td>{{ $pelanggan->foto }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['pelanggans.destroy', $pelanggan->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('pelanggans.show', [$pelanggan->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('pelanggans.edit', [$pelanggan->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
