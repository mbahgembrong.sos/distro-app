<!-- Jenis Field -->
<div class="form-group col-sm-6">
    {!! Form::label('jenis', 'Jenis:') !!}
    {!! Form::select('jenis', $jenisKeuangan, null, [
        'class' => 'form-control custom-select',
    ]) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('nama', 'Nama :') !!}
    {!! Form::text('nama', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6"id="tagihan" style="display:none;">
    {!! Form::label('tagihan', 'Tagihan:') !!}
    <select class="form-control custom-select" id="tagihan" name="tagihan">
        <option value="" disabled selected hidden>Pilih tagihan</option>
        @foreach ($tagihans as $tagihan)
            <option value="{{ $tagihan->id }}" data-jumlah="{{ $tagihan->jumlah }}">{{ $tagihan->nama }}
            </option>
        @endforeach
    </select>
</div>
<div class="form-group col-sm-6" id="gaji-karyawan"style="display:none;">
    {!! Form::label('karyawan', 'Karyawan:') !!}
    <select class="form-control custom-select" id="karyawan" name="karyawan">
        <option value="" disabled selected hidden>Pilih gaji karyawan</option>
        @foreach ($karyawans as $karyawan)
            <option value="{{ $karyawan->id }}" data-gaji="{{ $karyawan->gaji }}">{{ $karyawan->nama }}
            </option>
        @endforeach
    </select>
</div>

<div class="row col-sm-12" id="lain-lain"style="display:none;">
    <!-- Keterangan Field -->
    <div class="form-group col-sm-12 col-lg-12">
        {!! Form::label('keterangan', 'Keterangan:') !!}
        {!! Form::textarea('keterangan', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group col-sm-6">
    {!! Form::label('total', 'Total :') !!}
    {!! Form::number('total', null, ['class' => 'form-control', 'readonly']) !!}
</div>

<!-- Foto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('foto', 'Foto:') !!}
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('foto', ['class' => 'custom-file-input']) !!}
            {!! Form::label('foto', 'Choose file', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
</div>
<div class="clearfix"></div>
@push('page_scripts')
    <script>
        $(document).ready(function() {
            $('select[name="jenis"]').on('change', (e) => {
                var jenis = e.target.value;
                if (jenis == 'tagihan') {
                    $('#tagihan').show();
                    $('#gaji-karyawan').hide();
                    $('#lain-lain').hide();
                    $('select[name="tagihan"]').on('change', (e) => {
                        var tagihan = e.target.value;
                        $('input[name="total"]').val($('select[name="tagihan"] option:selected')
                            .data('jumlah'));
                        $('input[name="nama"]').val('Tagihan ' + $(
                                'select[name="tagihan"] option:selected')
                            .text());
                    });
                } else if (jenis == 'gaji karyawan') {
                    $('#tagihan').hide();
                    $('#gaji-karyawan').show();
                    $('#lain-lain').hide();
                    $('select[name="karyawan"]').on('change', (e) => {
                        var karyawan = e.target.value;
                        $('input[name="total"]').val($('select[name="karyawan"] option:selected')
                            .data('gaji'));
                        $('input[name="nama"]').val('Bayar Gaji ' + $(
                                'select[name="karyawan"] option:selected')
                            .text());
                    });
                } else {
                    $('#tagihan').hide();
                    $('#gaji-karyawan').hide();
                    $('#lain-lain').show();
                }
            });
        });
    </script>
@endpush
