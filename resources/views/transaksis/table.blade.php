<div class="table-responsive">
    <table class="table" id="transaksis-table">
        <thead>
        <tr>
            <th>Pelanggan Id</th>
        <th>Kasir Id</th>
        <th>Status</th>
        <th>Grand Total</th>
        <th>Berat Total</th>
        <th>Expedisi</th>
        <th>Type Expedisi</th>
        <th>Ongkir</th>
        <th>Estimasi</th>
        <th>Resi Pengiriman</th>
        <th>Type Pembelian</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($transaksis as $transaksi)
            <tr>
                <td>{{ $transaksi->pelanggan_id }}</td>
            <td>{{ $transaksi->kasir_id }}</td>
            <td>{{ $transaksi->status }}</td>
            <td>{{ $transaksi->grand_total }}</td>
            <td>{{ $transaksi->berat_total }}</td>
            <td>{{ $transaksi->expedisi }}</td>
            <td>{{ $transaksi->type_expedisi }}</td>
            <td>{{ $transaksi->ongkir }}</td>
            <td>{{ $transaksi->estimasi }}</td>
            <td>{{ $transaksi->resi_pengiriman }}</td>
            <td>{{ $transaksi->type_pembelian }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['transaksis.destroy', $transaksi->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('transaksis.show', [$transaksi->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('transaksis.edit', [$transaksi->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
