<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Karyawan;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class KaryawanAuthController extends Controller
{
    // use AuthenticatesUsers;

    protected $maxAttempts = 3;
    protected $decayMinutes = 2;

    public function __construct()
    {
        $this->middleware('guest:karyawan')->except('postLogout');
    }

    public function getLogin()
    {
        return view('auth.karyawan.login');
    }

    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if (auth()->guard('karyawan')->attempt($request->only('email', 'password'))) {
            $karyawan = Karyawan::where('email', $request->email)->first();
            $request->session()->regenerate();
            // $this->clearLoginAttempts($request);
            return redirect()->intended('/laporan');
        } else {
            $this->incrementLoginAttempts($request);

            return redirect()
                ->back()
                ->withInput()
                ->withErrors(["Incorrect user login details!"]);
        }
    }

    public function postLogout()
    {
        auth()->guard('karyawan')->logout();
        session()->flush();
        Auth::logout();

        return redirect()->route('karyawan.login');
    }
}
