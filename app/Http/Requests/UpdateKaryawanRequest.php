<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Karyawan;

class UpdateKaryawanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Karyawan::$rules;
        $rules['email'] = $rules['email'].",".$this->route("karyawan");$rules['telp'] = $rules['telp'].",".$this->route("karyawan");
        return $rules;
    }
}
