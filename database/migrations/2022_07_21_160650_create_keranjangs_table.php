<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKeranjangsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('keranjangs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('pelanggan_id');
            $table->unsignedInteger('produk_id');
            $table->unsignedInteger('detail_ukuran_produk_id');
            $table->integer('jumlah');
            $table->integer('total');
            $table->timestamps();
        });
        Schema::table('keranjangs', function (Blueprint  $table) {
            $table->foreign('detail_ukuran_produk_id')->references('id')->on('detail_ukuran_produks')->onDelete('cascade');
            $table->foreign('produk_id')->references('id')->on('produks')->onDelete('cascade');
            $table->foreign('pelanggan_id')->references('id')->on('pelanggans')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('keranjangs');
    }
}
