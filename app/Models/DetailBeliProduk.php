<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class DetailBeliProduk
 * @package App\Models
 * @version July 21, 2022, 2:58 pm UTC
 *
 * @property unsignedBigInteger $detail_ukuran_produk_id
 * @property integer $stock
 * @property integer $harga_beli
 */
class DetailBeliProduk extends Model
{

    use HasFactory;

    public $table = 'detail_beli_produks';




    public $fillable = [
        'detail_ukuran_produk_id',
        'detail_keuangan_keluar_id',
        'stock',
        'harga_beli'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    // protected $casts = [
    //     'id' => 'integer',
    //     'stock' => 'integer',
    //     'harga_beli' => 'integer'
    // ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'stock' => 'array',
        'stock.*' => 'numeric',
        'harga_beli' => 'array',
        'harga_beli.*' => 'numeric',
        'foto' => 'required|file'
    ];
    public function detailUkuranProduk()
    {
        return $this->belongsTo('App\Models\DetailUkuranProduk', 'detail_ukuran_produk_id');
    }
}
