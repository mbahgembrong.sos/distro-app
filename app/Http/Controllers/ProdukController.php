<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProdukRequest;
use App\Http\Requests\UpdateProdukRequest;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\CreateDetailBeliProdukRequest;
use App\Models\DetailUkuranProduk;
use App\Models\Kategori;
use App\Models\Produk;
use CreateDetailBeliProduksTable;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Storage;
use Response;

class ProdukController extends AppBaseController
{
    /**
     * Display a listing of the Produk.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Produk $produks */
        $produks = Produk::all();

        return view('produks.index')
            ->with('produks', $produks);
    }

    /**
     * Show the form for creating a new Produk.
     *
     * @return Response
     */
    public function create()
    {
        $kategoris = Kategori::pluck('nama', 'id');
        return view('produks.create', compact('kategoris'));
    }

    /**
     * Store a newly created Produk in storage.
     *
     * @param CreateProdukRequest $request
     *
     * @return Response
     */
    public function store(CreateProdukRequest $request)
    {
        $input = $request->all();
        Storage::disk('public')->put('produk/' . $request->foto->getClientOriginalName(), file_get_contents($request->foto));
        $input['foto'] = $request->foto->getClientOriginalName();
        /** @var Produk $produk */
        $produk = Produk::create($input);
        foreach ($request->ukuran as $item) {
            if ($item != null) {
                $detail_ukuran_produk = new \App\Models\DetailUkuranProduk();
                $detail_ukuran_produk->ukuran = $item;
                $detail_ukuran_produk->produk_id = $produk->id;
                $detail_ukuran_produk->harga = 0;
                $detail_ukuran_produk->save();
            }
        }

        Flash::success('Produk saved successfully.');

        return redirect(route('produks.index'));
    }

    /**
     * Display the specified Produk.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Produk $produk */
        $produk = Produk::find($id)->with('kategori', 'detailUkuranProduk')->first();

        if (empty($produk)) {
            Flash::error('Produk not found');

            return redirect(route('produks.index'));
        }

        return view('produks.show')->with('produk', $produk);
    }
    public function add_stock(CreateDetailBeliProdukRequest $request)
    {
        Storage::disk('public')->put('keuangan-keluar/' . $request->foto->getClientOriginalName(), file_get_contents($request->foto));
        $keuanganKeluar = new \App\Models\KeuanganKeluar();
        $keuanganKeluar->nama = 'Pembelian Produk ' . $request->produk_nama;
        $keuanganKeluar->jenis = 'pembelian produk';
        $keuanganKeluar->total = 0;
        $keuanganKeluar->foto = $request->foto->getClientOriginalName();
        $keuanganKeluar->save();
        $total = 0;
        foreach ($request->stock as $key => $stock) {
            if ($stock != null && $request->harga_beli[$key] != null) {
                $detailBeliProduk = new \App\Models\DetailBeliProduk();
                $detailBeliProduk->stock = $stock;
                $detailBeliProduk->harga_beli = $request->harga_beli[$key];
                $detailBeliProduk->keuangan_keluar_id = $keuanganKeluar->id;
                $detailBeliProduk->detail_ukuran_produk_id = $request->detail_ukuran_produk_id[$key];
                $detailBeliProduk->save();
                $total += $request->harga_beli[$key] * $stock;
                $detail_ukuran_produk = \App\Models\DetailUkuranProduk::find($request->detail_ukuran_produk_id[$key]);
                $detail_ukuran_produk->stock = $detail_ukuran_produk->stock + $stock;
                $detail_ukuran_produk->save();
            }
        }
        $keuanganKeluar->total = $total;
        $keuanganKeluar->save();
        Flash::success('Produk add stock successfully.');

        return redirect(route('produks.index'));
    }
    public function harga_jual($id)
    {
        $produk = Produk::find($id)->with('kategori', 'detailUkuranProduk')->first();

        if (empty($produk)) {
            Flash::error('Produk not found');

            return redirect(route('produks.index'));
        }

        return view('produks.harga_jual')->with('produk', $produk);
    }
    function update_harga_jual(Request $request, $id)
    {
        $produk = Produk::find($id)->first();

        if (empty($produk)) {
            Flash::error('Produk not found');

            return redirect(route('produks.index'));
        }


        foreach ($request->harga as $key => $item) {
            if ($item != null) {
                $detail_ukuran_produk = DetailUkuranProduk::find($request->detail_ukuran_produk_id[$key]);
                $detail_ukuran_produk->harga = $item;
                $detail_ukuran_produk->save();
            }
        }
        if (auth()->guard('karyawan')->user()->role()->first()->nama == 'Pemilik') {
            $produk->validasi = 1;
            $produk->save();
            Flash::success('Produk validasi harga jual successfully.');
        } else {
            $produk->validasi = 0;
            $produk->save();
            Flash::success('Produk update harga jual successfully.');
        }


        return redirect(route('produks.index'));
    }

    /**
     * Show the form for editing the specified Produk.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Produk $produk */
        $produk = Produk::find($id)->with('detailUkuranProduk')->first();
        if (empty($produk)) {
            Flash::error('Produk not found');

            return redirect(route('produks.index'));
        }

        $kategoris = Kategori::pluck('nama', 'id');

        return view('produks.edit', compact('kategoris'))->with('produk', $produk);
    }

    /**
     * Update the specified Produk in storage.
     *
     * @param int $id
     * @param UpdateProdukRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProdukRequest $request)
    {
        /** @var Produk $produk */
        $produk = Produk::find($id);

        if (empty($produk)) {
            Flash::error('Produk not found');

            return redirect(route('produks.index'));
        }
        $produk->fill($request->all());
        if ($request->hasFile('foto')) {
            Storage::disk('public')->put('produk/' . $request->foto->getClientOriginalName(), file_get_contents($request->foto));
            $produk->foto = $request->foto->getClientOriginalName();
        }
        $produk->save();

        $detail_ukuran_produk = \App\Models\DetailUkuranProduk::where('produk_id', $produk->id)->delete();
        foreach ($request->ukuran as $item) {
            if ($item != null) {
                $detail_ukuran_produk = new \App\Models\DetailUkuranProduk();
                $detail_ukuran_produk->ukuran = $item;
                $detail_ukuran_produk->produk_id = $produk->id;
                $detail_ukuran_produk->harga = 0;
                $detail_ukuran_produk->save();
            }
        }

        Flash::success('Produk updated successfully.');

        return redirect(route('produks.index'));
    }

    /**
     * Remove the specified Produk from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Produk $produk */
        $produk = Produk::find($id);

        if (empty($produk)) {
            Flash::error('Produk not found');

            return redirect(route('produks.index'));
        }

        $produk->delete();

        Flash::success('Produk deleted successfully.');

        return redirect(route('produks.index'));
    }
}
