<?php

namespace Database\Factories;

use App\Models\Tagihanan;
use Illuminate\Database\Eloquent\Factories\Factory;

class TagihananFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Tagihanan::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nama' => $this->faker->word,
        'jenis_tagihan' => $this->faker->word,
        'jumlah' => $this->faker->randomDigitNotNull,
        'scedjule' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
