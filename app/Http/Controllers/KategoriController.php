<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateKategoriRequest;
use App\Http\Requests\UpdateKategoriRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Kategori;
use Illuminate\Http\Request;
use Flash;
use Response;

class KategoriController extends AppBaseController
{
    /**
     * Display a listing of the Kategori.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Kategori $kategoris */
        $kategoris = Kategori::all();

        return view('kategoris.index')
            ->with('kategoris', $kategoris);
    }

    /**
     * Show the form for creating a new Kategori.
     *
     * @return Response
     */
    public function create()
    {
        return view('kategoris.create');
    }

    /**
     * Store a newly created Kategori in storage.
     *
     * @param CreateKategoriRequest $request
     *
     * @return Response
     */
    public function store(CreateKategoriRequest $request)
    {
        $input = $request->all();

        /** @var Kategori $kategori */
        $kategori = Kategori::create($input);

        Flash::success('Kategori saved successfully.');

        return redirect(route('kategoris.index'));
    }

    /**
     * Display the specified Kategori.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Kategori $kategori */
        $kategori = Kategori::find($id);

        if (empty($kategori)) {
            Flash::error('Kategori not found');

            return redirect(route('kategoris.index'));
        }

        return view('kategoris.show')->with('kategori', $kategori);
    }

    /**
     * Show the form for editing the specified Kategori.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Kategori $kategori */
        $kategori = Kategori::find($id);

        if (empty($kategori)) {
            Flash::error('Kategori not found');

            return redirect(route('kategoris.index'));
        }

        return view('kategoris.edit')->with('kategori', $kategori);
    }

    /**
     * Update the specified Kategori in storage.
     *
     * @param int $id
     * @param UpdateKategoriRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateKategoriRequest $request)
    {
        /** @var Kategori $kategori */
        $kategori = Kategori::find($id);

        if (empty($kategori)) {
            Flash::error('Kategori not found');

            return redirect(route('kategoris.index'));
        }

        $kategori->fill($request->all());
        $kategori->save();

        Flash::success('Kategori updated successfully.');

        return redirect(route('kategoris.index'));
    }

    /**
     * Remove the specified Kategori from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Kategori $kategori */
        $kategori = Kategori::find($id);

        if (empty($kategori)) {
            Flash::error('Kategori not found');

            return redirect(route('kategoris.index'));
        }

        $kategori->delete();

        Flash::success('Kategori deleted successfully.');

        return redirect(route('kategoris.index'));
    }
}
