<?php

namespace Database\Factories;

use App\Models\DetailBeliProduk;
use Illuminate\Database\Eloquent\Factories\Factory;

class DetailBeliProdukFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DetailBeliProduk::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'detail_ukuran_produk_id' => $this->faker->word,
        'keuangan_keluar_id' => $this->faker->word,
        'stock' => $this->faker->randomDigitNotNull,
        'harga_beli' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
