<?php

namespace Database\Factories;

use App\Models\KeuanganKeluar;
use Illuminate\Database\Eloquent\Factories\Factory;

class KeuanganKeluarFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = KeuanganKeluar::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'jenis' => $this->faker->word,
        'total' => $this->faker->randomDigitNotNull,
        'keterangan' => $this->faker->text,
        'foto' => $this->faker->word,
        'validasi' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
