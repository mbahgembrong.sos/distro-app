<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateKeranjangRequest;
use App\Http\Requests\UpdateKeranjangRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Keranjang;
use Illuminate\Http\Request;
use Flash;
use Response;

class KeranjangController extends AppBaseController
{
    /**
     * Display a listing of the Keranjang.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Keranjang $keranjangs */
        $keranjangs = Keranjang::all();

        return view('keranjangs.index')
            ->with('keranjangs', $keranjangs);
    }

    /**
     * Show the form for creating a new Keranjang.
     *
     * @return Response
     */
    public function create()
    {
        $produks = \App\Models\Produk::all()->pluck('nama', 'id');
        $pelanggans = \App\Models\Pelanggan::all()->pluck('nama', 'id');
        $detail_ukuran_produks = \App\Models\DetailUkuranProduk::all()->pluck('nama', 'id');
        return view('keranjangs.create',compact('produks','pelanggans','detail_ukuran_produks'));
    }

    /**
     * Store a newly created Keranjang in storage.
     *
     * @param CreateKeranjangRequest $request
     *
     * @return Response
     */
    public function store(CreateKeranjangRequest $request)
    {
        $input = $request->all();

        /** @var Keranjang $keranjang */
        $keranjang = Keranjang::create($input);

        Flash::success('Keranjang saved successfully.');

        return redirect(route('keranjangs.index'));
    }

    /**
     * Display the specified Keranjang.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Keranjang $keranjang */
        $keranjang = Keranjang::find($id);

        if (empty($keranjang)) {
            Flash::error('Keranjang not found');

            return redirect(route('keranjangs.index'));
        }

        return view('keranjangs.show')->with('keranjang', $keranjang);
    }

    /**
     * Show the form for editing the specified Keranjang.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Keranjang $keranjang */
        $keranjang = Keranjang::find($id);

        if (empty($keranjang)) {
            Flash::error('Keranjang not found');

            return redirect(route('keranjangs.index'));
        }
        $produks = \App\Models\Produk::all()->pluck('nama', 'id');
        $pelanggans = \App\Models\Pelanggan::all()->pluck('nama', 'id');
        $detail_ukuran_produks = \App\Models\DetailUkuranProduk::all()->pluck('nama', 'id');

        return view('keranjangs.edit',compact('produks','pelanggans','detail_ukuran_produks'))->with('keranjang', $keranjang);
    }

    /**
     * Update the specified Keranjang in storage.
     *
     * @param int $id
     * @param UpdateKeranjangRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateKeranjangRequest $request)
    {
        /** @var Keranjang $keranjang */
        $keranjang = Keranjang::find($id);

        if (empty($keranjang)) {
            Flash::error('Keranjang not found');

            return redirect(route('keranjangs.index'));
        }

        $keranjang->fill($request->all());
        $keranjang->save();

        Flash::success('Keranjang updated successfully.');

        return redirect(route('keranjangs.index'));
    }

    /**
     * Remove the specified Keranjang from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Keranjang $keranjang */
        $keranjang = Keranjang::find($id);

        if (empty($keranjang)) {
            Flash::error('Keranjang not found');

            return redirect(route('keranjangs.index'));
        }

        $keranjang->delete();

        Flash::success('Keranjang deleted successfully.');

        return redirect(route('keranjangs.index'));
    }
}
