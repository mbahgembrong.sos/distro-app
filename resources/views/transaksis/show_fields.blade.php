<!-- Pelanggan Id Field -->
<div class="col-sm-12">
    {!! Form::label('pelanggan_id', 'Pelanggan Id:') !!}
    <p>{{ $transaksi->pelanggan_id }}</p>
</div>

<!-- Kasir Id Field -->
<div class="col-sm-12">
    {!! Form::label('kasir_id', 'Kasir Id:') !!}
    <p>{{ $transaksi->kasir_id }}</p>
</div>

<!-- Status Field -->
<div class="col-sm-12">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $transaksi->status }}</p>
</div>

<!-- Grand Total Field -->
<div class="col-sm-12">
    {!! Form::label('grand_total', 'Grand Total:') !!}
    <p>{{ $transaksi->grand_total }}</p>
</div>

<!-- Berat Total Field -->
<div class="col-sm-12">
    {!! Form::label('berat_total', 'Berat Total:') !!}
    <p>{{ $transaksi->berat_total }}</p>
</div>

<!-- Expedisi Field -->
<div class="col-sm-12">
    {!! Form::label('expedisi', 'Expedisi:') !!}
    <p>{{ $transaksi->expedisi }}</p>
</div>

<!-- Type Expedisi Field -->
<div class="col-sm-12">
    {!! Form::label('type_expedisi', 'Type Expedisi:') !!}
    <p>{{ $transaksi->type_expedisi }}</p>
</div>

<!-- Ongkir Field -->
<div class="col-sm-12">
    {!! Form::label('ongkir', 'Ongkir:') !!}
    <p>{{ $transaksi->ongkir }}</p>
</div>

<!-- Estimasi Field -->
<div class="col-sm-12">
    {!! Form::label('estimasi', 'Estimasi:') !!}
    <p>{{ $transaksi->estimasi }}</p>
</div>

<!-- Resi Pengiriman Field -->
<div class="col-sm-12">
    {!! Form::label('resi_pengiriman', 'Resi Pengiriman:') !!}
    <p>{{ $transaksi->resi_pengiriman }}</p>
</div>

<!-- Type Pembelian Field -->
<div class="col-sm-12">
    {!! Form::label('type_pembelian', 'Type Pembelian:') !!}
    <p>{{ $transaksi->type_pembelian }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $transaksi->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $transaksi->updated_at }}</p>
</div>

