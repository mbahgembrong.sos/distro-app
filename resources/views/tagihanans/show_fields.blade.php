<!-- Nama Field -->
<div class="col-sm-12">
    {!! Form::label('nama', 'Nama:') !!}
    <p>{{ $tagihanan->nama }}</p>
</div>

<!-- Jenis Tagihan Field -->
<div class="col-sm-12">
    {!! Form::label('jenis_tagihan', 'Jenis Tagihan:') !!}
    <p>{{ $tagihanan->jenis_tagihan }}</p>
</div>

<!-- Jumlah Field -->
<div class="col-sm-12">
    {!! Form::label('jumlah', 'Jumlah:') !!}
    <p>{{ $tagihanan->jumlah }}</p>
</div>

<!-- Scedjule Field -->
<div class="col-sm-12">
    {!! Form::label('scedjule', 'Scedjule:') !!}
    <p>{{ date_format(date_create("2022-".$tagihanan->scedjule."-01"),"F") }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $tagihanan->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $tagihanan->updated_at }}</p>
</div>

