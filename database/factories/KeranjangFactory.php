<?php

namespace Database\Factories;

use App\Models\Keranjang;
use Illuminate\Database\Eloquent\Factories\Factory;

class KeranjangFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Keranjang::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'pelanggan_id' => $this->faker->word,
        'produk_id' => $this->faker->word,
        'detail_ukuran_produk_id' => $this->faker->word,
        'jumlah' => $this->faker->randomDigitNotNull,
        'total' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
