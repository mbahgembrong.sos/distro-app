<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateKaryawanRequest;
use App\Http\Requests\UpdateKaryawanRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Karyawan;
use App\Models\Role;
use Illuminate\Http\Request;
use Flash;
use Response;

class KaryawanController extends AppBaseController
{
    /**
     * Display a listing of the Karyawan.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Karyawan $karyawans */
        $karyawans = Karyawan::all();

        return view('karyawans.index')
            ->with('karyawans', $karyawans);
    }

    /**
     * Show the form for creating a new Karyawan.
     *
     * @return Response
     */
    public function create()
    {
        $roles = Role::pluck('nama', 'id');
        return view('karyawans.create', compact('roles'));
    }

    /**
     * Store a newly created Karyawan in storage.
     *
     * @param CreateKaryawanRequest $request
     *
     * @return Response
     */
    public function store(CreateKaryawanRequest $request)
    {
        $input = $request->all();

        /** @var Karyawan $karyawan */
        $input['password'] = bcrypt($input['password']);
        $karyawan = Karyawan::create($input);

        Flash::success('Karyawan saved successfully.');

        return redirect(route('karyawans.index'));
    }

    /**
     * Display the specified Karyawan.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Karyawan $karyawan */
        $karyawan = Karyawan::find($id);

        if (empty($karyawan)) {
            Flash::error('Karyawan not found');

            return redirect(route('karyawans.index'));
        }

        return view('karyawans.show')->with('karyawan', $karyawan);
    }

    /**
     * Show the form for editing the specified Karyawan.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Karyawan $karyawan */
        $karyawan = Karyawan::find($id);

        if (empty($karyawan)) {
            Flash::error('Karyawan not found');

            return redirect(route('karyawans.index'));
        }
        $roles = Role::pluck('nama', 'id');

        return view('karyawans.edit',compact('roles'))->with('karyawan', $karyawan);
    }

    /**
     * Update the specified Karyawan in storage.
     *
     * @param int $id
     * @param UpdateKaryawanRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateKaryawanRequest $request)
    {
        /** @var Karyawan $karyawan */
        $karyawan = Karyawan::find($id);

        if (empty($karyawan)) {
            Flash::error('Karyawan not found');

            return redirect(route('karyawans.index'));
        }
        $request['password'] = bcrypt($request->password);
        $karyawan->fill($request->all());
        $karyawan->save();

        Flash::success('Karyawan updated successfully.');

        return redirect(route('karyawans.index'));
    }

    /**
     * Remove the specified Karyawan from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Karyawan $karyawan */
        $karyawan = Karyawan::find($id);

        if (empty($karyawan)) {
            Flash::error('Karyawan not found');

            return redirect(route('karyawans.index'));
        }

        $karyawan->delete();

        Flash::success('Karyawan deleted successfully.');

        return redirect(route('karyawans.index'));
    }
}
