<!-- Nama Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nama', 'Nama:') !!}
    {!! Form::text('nama', null, ['class' => 'form-control']) !!}
</div>

<!-- Jenis Tagihan Field -->
<div class="form-group col-sm-6">
    {!! Form::label('jenis_tagihan', 'Jenis Tagihan:') !!}
    {!! Form::select('jenis_tagihan', ['Bulan' => 'Bulan', 'Tahun' => 'Tahun'], null, ['class' => 'form-control']) !!}
</div>

<!-- Jumlah Field -->
<div class="form-group col-sm-6">
    {!! Form::label('jumlah', 'Jumlah:') !!}
    {!! Form::number('jumlah', null, ['class' => 'form-control']) !!}
</div>

<!-- Scedjule Field -->
<div class="form-group col-sm-6">
    {!! Form::label('scedjule', 'Scedjule:') !!}
    {!! Form::text('scedjule', null, ['class' => 'form-control', 'id' => 'scedjule']) !!}
</div>

@push('page_scripts')
    <script type="text/javascript">
        $('#scedjule').datetimepicker({
            format: 'MM-DD',
            useCurrent: true,
            sideBySide: true,

        })
    </script>
@endpush
