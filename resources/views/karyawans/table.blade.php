<div class="table-responsive">
    <table class="table" id="karyawans-table">
        <thead>
        <tr>
            <th>Role Id</th>
        <th>Nama</th>
        <th>Email</th>
        <th>Telp</th>
        <th>Alamat</th>
        <th>Gaji</th>
        <th>Scedjule</th>
        <th>Foto</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($karyawans as $karyawan)
            <tr>
                <td>{{ $karyawan->role_id }}</td>
            <td>{{ $karyawan->nama }}</td>
            <td>{{ $karyawan->email }}</td>
            <td>{{ $karyawan->telp }}</td>
            <td>{{ $karyawan->alamat }}</td>
            <td>{{ $karyawan->gaji }}</td>
            <td>{{ $karyawan->scedjule }}</td>
            <td>{{ $karyawan->foto }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['karyawans.destroy', $karyawan->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('karyawans.show', [$karyawan->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('karyawans.edit', [$karyawan->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
