<?php

namespace Database\Factories;

use App\Models\Transaksi;
use Illuminate\Database\Eloquent\Factories\Factory;

class TransaksiFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Transaksi::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'pelanggan_id' => $this->faker->word,
        'kasir_id' => $this->faker->word,
        'status' => $this->faker->word,
        'grand_total' => $this->faker->randomDigitNotNull,
        'berat_total' => $this->faker->randomDigitNotNull,
        'expedisi' => $this->faker->word,
        'type_expedisi' => $this->faker->word,
        'ongkir' => $this->faker->randomDigitNotNull,
        'estimasi' => $this->faker->randomDigitNotNull,
        'resi_pengiriman' => $this->faker->word,
        'type_pembelian' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
