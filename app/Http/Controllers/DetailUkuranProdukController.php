<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDetailUkuranProdukRequest;
use App\Http\Requests\UpdateDetailUkuranProdukRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\DetailUkuranProduk;
use Illuminate\Http\Request;
use Flash;
use Response;

class DetailUkuranProdukController extends AppBaseController
{
    /**
     * Display a listing of the DetailUkuranProduk.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var DetailUkuranProduk $detailUkuranProduks */
        $detailUkuranProduks = DetailUkuranProduk::all();

        return view('detail_ukuran_produks.index')
            ->with('detailUkuranProduks', $detailUkuranProduks);
    }

    /**
     * Show the form for creating a new DetailUkuranProduk.
     *
     * @return Response
     */
    public function create()
    {
        return view('detail_ukuran_produks.create');
    }

    /**
     * Store a newly created DetailUkuranProduk in storage.
     *
     * @param CreateDetailUkuranProdukRequest $request
     *
     * @return Response
     */
    public function store(CreateDetailUkuranProdukRequest $request)
    {
        $input = $request->all();

        /** @var DetailUkuranProduk $detailUkuranProduk */
        $detailUkuranProduk = DetailUkuranProduk::create($input);

        Flash::success('Detail Ukuran Produk saved successfully.');

        return redirect(route('detailUkuranProduks.index'));
    }

    /**
     * Display the specified DetailUkuranProduk.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var DetailUkuranProduk $detailUkuranProduk */
        $detailUkuranProduk = DetailUkuranProduk::find($id);

        if (empty($detailUkuranProduk)) {
            Flash::error('Detail Ukuran Produk not found');

            return redirect(route('detailUkuranProduks.index'));
        }

        return view('detail_ukuran_produks.show')->with('detailUkuranProduk', $detailUkuranProduk);
    }

    /**
     * Show the form for editing the specified DetailUkuranProduk.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var DetailUkuranProduk $detailUkuranProduk */
        $detailUkuranProduk = DetailUkuranProduk::find($id);

        if (empty($detailUkuranProduk)) {
            Flash::error('Detail Ukuran Produk not found');

            return redirect(route('detailUkuranProduks.index'));
        }

        return view('detail_ukuran_produks.edit')->with('detailUkuranProduk', $detailUkuranProduk);
    }

    /**
     * Update the specified DetailUkuranProduk in storage.
     *
     * @param int $id
     * @param UpdateDetailUkuranProdukRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDetailUkuranProdukRequest $request)
    {
        /** @var DetailUkuranProduk $detailUkuranProduk */
        $detailUkuranProduk = DetailUkuranProduk::find($id);

        if (empty($detailUkuranProduk)) {
            Flash::error('Detail Ukuran Produk not found');

            return redirect(route('detailUkuranProduks.index'));
        }

        $detailUkuranProduk->fill($request->all());
        $detailUkuranProduk->save();

        Flash::success('Detail Ukuran Produk updated successfully.');

        return redirect(route('detailUkuranProduks.index'));
    }

    /**
     * Remove the specified DetailUkuranProduk from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var DetailUkuranProduk $detailUkuranProduk */
        $detailUkuranProduk = DetailUkuranProduk::find($id);

        if (empty($detailUkuranProduk)) {
            Flash::error('Detail Ukuran Produk not found');

            return redirect(route('detailUkuranProduks.index'));
        }

        $detailUkuranProduk->delete();

        Flash::success('Detail Ukuran Produk deleted successfully.');

        return redirect(route('detailUkuranProduks.index'));
    }
}
