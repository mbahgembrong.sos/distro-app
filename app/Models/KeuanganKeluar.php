<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class KeuanganKeluar
 * @package App\Models
 * @version July 21, 2022, 2:27 pm UTC
 *
 * @property string $jenis
 * @property integer $total
 * @property string $keterangan
 * @property string $foto
 */
class KeuanganKeluar extends Model
{

    use HasFactory;

    public $table = 'keuangan_keluars';




    public $fillable = [
        'nama',
        'jenis',
        'total',
        'keterangan',
        'foto',
        'validasi',
        'foregin_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'jenis' => 'string',
        'total' => 'integer',
        'keterangan' => 'string',
        'foto' => 'string',
        'validasi' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'jenis' => 'required|string',
        'total' => 'required|numeric',
        'keterangan' => 'nullable|string',
        'foto' => 'required|file'
    ];

    /**
     * Get all of the detailBeliProduk for the KeuanganKeluar
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function detailBeliProduk()
    {
        return $this->hasMany(DetailBeliProduk::class, 'keuangan_keluar_id', 'id');
    }
}
