<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Transaksi
 * @package App\Models
 * @version July 21, 2022, 2:34 pm UTC
 *
 * @property unsignedBigInteger $pelanggan_id
 * @property unsignedBigInteger $kasir_id
 * @property string $status
 * @property integer $grand_total
 * @property integer $berat_total
 * @property string $expedisi
 * @property string $type_expedisi
 * @property integer $ongkir
 * @property integer $estimasi
 * @property string $resi_pengiriman
 * @property string $type_pembelian
 */
class Transaksi extends Model
{

    use HasFactory;

    public $table = 'transaksis';




    public $fillable = [
        'pelanggan_id',
        'kasir_id',
        'status',
        'grand_total',
        'berat_total',
        'expedisi',
        'type_expedisi',
        'ongkir',
        'estimasi',
        'resi_pengiriman',
        'type_pembelian',
        'validasi'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'status' => 'string',
        'grand_total' => 'integer',
        'berat_total' => 'integer',
        'expedisi' => 'string',
        'type_expedisi' => 'string',
        'ongkir' => 'integer',
        'estimasi' => 'integer',
        'resi_pengiriman' => 'string',
        'type_pembelian' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'pelanggan_id' => 'required',
        'kasir_id' => 'nullable',
        'status' => 'nullable',
        'expedisi' => 'nullable',
        'type_expedisi' => 'nullable',
        'ongkir' => 'nullable:integer',
        'estimasi' => 'nullable:integer',
        'resi_pengiriman' => 'nullable'
    ];

    /**
     * Get all of the detailTransaksi for the Transaksi
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function detailTransaksi(): HasMany
    {
        return $this->hasMany(DetailTransaksi::class, 'transaksi_id', 'id');
    }
    /**
     * Get all of the detailPembayaran for the Transaksi
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function detailPembayaran(): HasMany
    {
        return $this->hasMany(DetailPembayaran::class, 'transaksi_id', 'id');
    }
}
