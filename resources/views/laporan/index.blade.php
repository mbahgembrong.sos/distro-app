@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Laporan Pendapatan</h1>
                </div>
            </div>
        </div>
    </section>

    <div class="content px-3">

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="card">
            <div class="card-header border-0">
                <div class="d-flex justify-content-between">
                    <h3 class="card-title">Graph Laporan Pendapatan (Bulan)</h3>
                    <a href="javascript:void(0);"></a>
                </div>
            </div>
            <div class="position-relative mb-4">
                <canvas id="keuangan-chart" height="80"></canvas>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body p-0">
            @include('laporan.table')

            <div class="card-footer clearfix">
                <div class="float-right">

                </div>
            </div>
        </div>

    </div>
    </div>
@endsection
@push('page_scripts')
    <script>
        const ctx = document.getElementById('keuangan-chart').getContext('2d');
        const myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: {!! json_encode($months) !!},
                datasets: [{
                    label: 'Laporan Pendapatan',
                    data: {!! $graphLaporan !!},
                    backgroundColor: 'rgba(255, 99, 132, 0.2)',
                    borderColor: 'rgba(255, 99, 132, 1)',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
    </script>
@endpush
