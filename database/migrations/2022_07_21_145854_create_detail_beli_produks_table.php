<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailBeliProduksTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_beli_produks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('detail_ukuran_produk_id');
            $table->unsignedInteger('keuangan_keluar_id');
            $table->integer('stock');
            $table->integer('harga_beli');
            $table->timestamps();
        });
        Schema::table('detail_beli_produks', function (Blueprint  $table) {
            $table->foreign('detail_ukuran_produk_id')->references('id')->on('detail_ukuran_produks')->onDelete('cascade');
            $table->foreign('keuangan_keluar_id')->references('id')->on('keuangan_keluars')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detail_beli_produks');
    }
}
