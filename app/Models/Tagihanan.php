<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Tagihanan
 * @package App\Models
 * @version July 21, 2022, 2:23 pm UTC
 *
 * @property string $nama
 * @property string $jenis_tagihan
 * @property integer $jumlah
 * @property string $scedjule
 */
class Tagihanan extends Model
{

    use HasFactory;

    public $table = 'tagihanans';




    public $fillable = [
        'nama',
        'jenis_tagihan',
        'jumlah',
        'scedjule'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nama' => 'string',
        'jenis_tagihan' => 'string',
        'jumlah' => 'integer',
        'scedjule' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nama' => 'required|string',
        'jenis_tagihan' => 'nullable|string',
        'jumlah' => 'required|integer',
        'scedjule' => 'required'
    ];


}
