<!-- Pelanggan Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pelanggan_id', 'Pelanggan Id:') !!}
    {!! Form::select('pelanggan_id', $pelanggans, null, ['class' => 'form-control custom-select']) !!}
</div>


{{-- <!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::select('status', ['bayar','belum bayar', 'kemas','kirim','terima'], null, ['class' => 'form-control custom-select']) !!}
</div> --}}
<div class="form-group col-sm-12" id="transactions">
    {!! Form::label('item', 'Produk :') !!}
    <div class="form-group fieldGroup" data-id="1">
        <div class="input-group">
            <select name="items[]" class="form-control">
                <option value="" disabled selected>Pilih Produk</option>
                @foreach ($produks as $item)
                    <option value="{{ $item->id }}" data-berat="{{ $item->berat }}"
                        data-ukuran="{{ $item->detailUkuranProduk()->pluck('ukuran', 'id') }}"
                        data-stock="{{ $item->detailUkuranProduk()->pluck('stock', 'id') }}"
                        data-harga="{{ $item->detailUkuranProduk()->pluck('harga', 'id') }}">
                        {{ $item->nama }}</option>
                @endforeach
            </select>
            {!! Form::select('ukuran[]', [], null, [
                'class' => 'form-control  custom-select',
                'placeholder' => 'Pilih Ukuran',
                'disabled',
            ]) !!}

            {!! Form::number('jumlah[]', null, ['class' => 'form-control custom-select', 'min' => 0, 'placeholder' => '']) !!}
            {!! Form::number('total[]', null, [
                'class' => 'form-control total',
                'min' => 0,
                'placeholder' => '',
                'readonly',
            ]) !!}
            <div class="input-group-addon ml-3">
                <a href="javascript:void(0)" class="btn btn-success addMore"><i class="fas fa-plus"></i></a>
            </div>
        </div>
    </div>
</div>
{{-- trash --}}
<div class="form-group fieldGroupCopy" style="display: none;">
    <div class="input-group">
        <select name="items[]" class="form-control">
            <option value="" disabled selected>Pilih Items</option>
            @foreach ($produks as $item)
                <option value="{{ $item->id }}">
                    {{ $item->nama }}</option>
            @endforeach
        </select>
        {!! Form::select('ukuran[]', [], null, [
            'class' => 'form-control  custom-select',
            'placeholder' => 'Pilih Ukuran',
            'disabled',
        ]) !!}
        {!! Form::number('jumlah[]', null, ['class' => 'form-control counts', 'min' => 0, 'placeholder' => '']) !!}
        {!! Form::number('total[]', null, [
            'class' => 'form-control total',
            'min' => 0,
            'placeholder' => '',
            'readonly',
        ]) !!}
        <div class="input-group-addon ml-3">
            <a href="javascript:void(0)" class="btn btn-danger remove"><i class="fas fa-trash"></i></a>
        </div>
    </div>
</div>

<!-- Ongkir Field -->
<div class="form-group col-sm-6">
    {!! Form::label('grand-total', 'Grand Total:') !!}
    {!! Form::number('grand-total', null, ['class' => 'form-control', 'disabled']) !!}
</div>
<!-- Ongkir Field -->
<div class="form-group col-sm-6">
    {!! Form::label('berat', 'Berat:') !!}
    {!! Form::number('berat', null, ['class' => 'form-control', 'disabled']) !!}
</div>

<!-- Estimasi Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pembayaran', 'Pembayaran :') !!}
    {!! Form::number('pembayaran', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('kembalian', 'Kembalian :') !!}
    {!! Form::number('kembalian', null, ['class' => 'form-control', 'disabled']) !!}
</div>
@push('page_scripts')
    <script>
        $(document).ready(function() {
            var hargaJual = 0;
            var ukuranStock = 0;
            var grandTotal = 0;
            var beratTotal = 0;
            var ukuran = 0;
            var stock = 0;
            var harga = 0;
            var count = 0;
            var berat = 0;
            $('.fieldGroup').on('change', 'select[name="items[]"]', function() {
                console.log($('.fieldGroup').find('select[name="items[]"]'));
                ukuran = $(this).find(':selected').data('ukuran');
                stock = $(this).find(':selected').data('stock');
                harga = $(this).find(':selected').data('harga');
                berat = $(this).find(':selected').data('berat');
                count = $(this).closest('.fieldGroup').find('input[name="count[]"]').val();
                console.log($(this));
                $.each(ukuran, function(i, item) {
                    $('.fieldGroup').find('select[name="ukuran[]"]').append(
                        new Option(item, i));
                });
                $(this).closest('.fieldGroup').find('select[name="ukuran[]"]').prop('disabled', false);
            });
            $('.fieldGroup').on('change', 'select[name="ukuran[]"]', function() {
                hargaJual = harga[$(this).val()];
                ukuranStock = stock[$(this).val()];
            });
            $('.fieldGroup').on('keyup', 'input[name="jumlah[]"]', function() {
                console.log($(this).val());
                if (parseInt(ukuranStock) < $(this).val()) {
                    alert('Stock tidak mencukupi');
                    $(this).closest('.fieldGroup').find('input[name="count[]"]').val(0);
                } else {
                    $(this).closest('.fieldGroup').find('input[name="total[]"]').val(
                        hargaJual * $(this).val());
                    grandTotal = 0;
                    $('.fieldGroup').find('input[name="total[]"]').each(function() {
                        grandTotal += parseInt($(this).val());
                    });
                    beratTotal = berat * $(this).val();
                    $('input[name="grand-total"]').val(grandTotal);
                    $('input[name="berat"]').val(beratTotal);
                    $('input[name="pembayaran"]').on('keyup', function() {
                        $('input[name="kembalian"]').val($(this).val() - grandTotal);
                    });
                }
            });
            // membatasi jumlah inputan
            var maxGroup = 10;

            //melakukan proses multiple input
            $(".addMore").click(function() {
                var layanansInput = $('body').find('.fieldGroup').length;
                if (layanansInput < maxGroup) {
                    var fieldHTML = '<div class="form-group fieldGroup" data-id="' + (layanansInput + 1) +
                        '">' + $(".fieldGroupCopy").html() + '</div>';
                    $('body').find('.fieldGroup:last').after(fieldHTML);
                } else {
                    alert('Maksimal ' + maxGroup + ' Layanan.');
                }
            });
            //remove fields group
            $("body").on("click", ".remove", function() {
                $(this).parents(".fieldGroup").remove();
            });
        });
    </script>
@endpush
