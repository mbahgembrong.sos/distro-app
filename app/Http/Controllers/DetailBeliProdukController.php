<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDetailBeliProdukRequest;
use App\Http\Requests\UpdateDetailBeliProdukRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\DetailBeliProduk;
use Illuminate\Http\Request;
use Flash;
use Response;

class DetailBeliProdukController extends AppBaseController
{
    /**
     * Display a listing of the DetailBeliProduk.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var DetailBeliProduk $detailBeliProduks */
        $detailBeliProduks = DetailBeliProduk::all();

        return view('detail_beli_produks.index')
            ->with('detailBeliProduks', $detailBeliProduks);
    }

    /**
     * Show the form for creating a new DetailBeliProduk.
     *
     * @return Response
     */
    public function create()
    {
        $detailUkuranProduks = \App\Models\DetailUkuranProduk::all()->pluck('nama_ukuran_produk', 'id');
        return view('detail_beli_produks.create', compact('detailUkuranProduks'));
    }

    /**
     * Store a newly created DetailBeliProduk in storage.
     *
     * @param CreateDetailBeliProdukRequest $request
     *
     * @return Response
     */
    public function store(CreateDetailBeliProdukRequest $request)
    {
        $input = $request->all();

        /** @var DetailBeliProduk $detailBeliProduk */
        $detailBeliProduk = DetailBeliProduk::create($input);

        Flash::success('Detail Beli Produk saved successfully.');

        return redirect(route('detailBeliProduks.index'));
    }

    /**
     * Display the specified DetailBeliProduk.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var DetailBeliProduk $detailBeliProduk */
        $detailBeliProduk = DetailBeliProduk::find($id);

        if (empty($detailBeliProduk)) {
            Flash::error('Detail Beli Produk not found');

            return redirect(route('detailBeliProduks.index'));
        }

        return view('detail_beli_produks.show')->with('detailBeliProduk', $detailBeliProduk);
    }

    /**
     * Show the form for editing the specified DetailBeliProduk.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var DetailBeliProduk $detailBeliProduk */
        $detailBeliProduk = DetailBeliProduk::find($id);

        if (empty($detailBeliProduk)) {
            Flash::error('Detail Beli Produk not found');

            return redirect(route('detailBeliProduks.index'));
        }
        $detailUkuranProduks = \App\Models\DetailUkuranProduk::all()->pluck('nama_ukuran_produk', 'id');
        return view('detail_beli_produks.edit',compact('detailUkuranProduks'))->with('detailBeliProduk', $detailBeliProduk);
    }

    /**
     * Update the specified DetailBeliProduk in storage.
     *
     * @param int $id
     * @param UpdateDetailBeliProdukRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDetailBeliProdukRequest $request)
    {
        /** @var DetailBeliProduk $detailBeliProduk */
        $detailBeliProduk = DetailBeliProduk::find($id);

        if (empty($detailBeliProduk)) {
            Flash::error('Detail Beli Produk not found');

            return redirect(route('detailBeliProduks.index'));
        }

        $detailBeliProduk->fill($request->all());
        $detailBeliProduk->save();

        Flash::success('Detail Beli Produk updated successfully.');

        return redirect(route('detailBeliProduks.index'));
    }

    /**
     * Remove the specified DetailBeliProduk from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var DetailBeliProduk $detailBeliProduk */
        $detailBeliProduk = DetailBeliProduk::find($id);

        if (empty($detailBeliProduk)) {
            Flash::error('Detail Beli Produk not found');

            return redirect(route('detailBeliProduks.index'));
        }

        $detailBeliProduk->delete();

        Flash::success('Detail Beli Produk deleted successfully.');

        return redirect(route('detailBeliProduks.index'));
    }
}
