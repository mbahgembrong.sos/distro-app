<div class="table-responsive">
    <table class="table" id="jurnal_harian-table">
        <thead>
            <tr>
                <th>Bulan</th>
                <th>Pemasukan</th>
                <th>Pengeluaran</th>
                <th>Pendapatan</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($laporanMonth as $laporan)
                <tr>
                    <td>{{ $laporan['bulan']. ' '.date('Y')}}</td>
                    <td>Rp. {{ $laporan['transaksi'] }}</td>
                    <td>Rp. {{ $laporan['pengeluaran'] }}</td>
                    <td>Rp. {{ $laporan['pendapatan'] }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
