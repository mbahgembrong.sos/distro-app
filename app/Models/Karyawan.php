<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

/**
 * Class Karyawan
 * @package App\Models
 * @version July 21, 2022, 2:18 pm UTC
 *
 * @property unsignedBigInteger $role_id
 * @property string $nama
 * @property string $email
 * @property string $password
 * @property string $telp
 * @property string $alamat
 * @property integer $gaji
 * @property string $scedjule
 * @property string|nullable $foto
 */
class Karyawan extends Authenticatable
{

    use HasApiTokens, HasFactory, Notifiable;
    public $table = 'karyawans';
    protected $guard = 'karyawan';




    public $fillable = [
        'role_id',
        'nama',
        'email',
        'password',
        'telp',
        'alamat',
        'gaji',
        'scedjule',
        'foto', 'email_verfied_at'
    ];
    protected $hidden = ['password', 'remember_token'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nama' => 'string',
        'email' => 'string',
        'password' => 'string',
        'telp' => 'string',
        'alamat' => 'string',
        'gaji' => 'integer',
        'scedjule' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'role_id' => 'required',
        'nama' => 'required|string',
        'email' => 'required|unique:karyawans,email',
        'password' => 'required|string',
        'telp' => 'required|numeric|unique:karyawans,telp',
        'alamat' => 'required|string',
        'gaji' => 'required|integer',
        'scedjule' => 'required',
        'foto' => 'nullable|file'
    ];

    /**
     * Get the role that owns the Karyawan
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }
    /**
     * Get all of the transaksi for the Karyawan
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transaksi()
    {
        return $this->hasMany(Transaksi::class, 'karyawan_id', 'id');
    }
}
