@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Validasi {{ $keuanganKeluar->nama }}</h1>
                </div>
                <div class="col-sm-6">
                    <a class="btn btn-default float-right" href="{{ route('keuanganKeluars.index') }}">
                        Back
                    </a>
                </div>
            </div>
        </div>
    </section>

    <div class="content px-3">
        <div class="card">
            {!! Form::open(['route' => ['keuanganKeluars.validasi',$keuanganKeluar->id], 'files' => true]) !!}
            <div class="card-body">
                <div class="row">
                    <div class="row col-sm-6">
                        <div class="col-sm-12">
                            {!! Form::label('nama', 'nama:') !!}
                            <p>{{ $keuanganKeluar->nama }}</p>
                        </div>
                        <div class="col-sm-12">
                            {!! Form::label('total', 'total:') !!}
                            <p>Rp. {{ $keuanganKeluar->total }}</p>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        {!! Form::label('bukti pembelian', 'bukti pembelian:') !!}
                        <div class="col-md-12">
                            <div class="form-group row justify-content-center">
                                <img class="boxed--rectangle--detail"
                                    src="{{ asset('storage/keuangan-keluar/' . $keuanganKeluar->foto) }}" />
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        {!! Form::label('pembelian', 'Pembelian :') !!}
                        @foreach ($keuanganKeluar->detailBeliProduk as $item)
                            <div class="form-group fieldGroup col-12" data-id="1">
                                <div class="input-group">
                                    {!! Form::text('', 'Produk : ' . $item->detailUkuranProduk->produk->nama, [
                                        'class' => 'form-control',
                                        'placeholder' => 'Stock',
                                        'readonly',
                                    ]) !!}
                                    {!! Form::text('', 'Ukuran : ' . $item->detailUkuranProduk->ukuran, [
                                        'class' => 'form-control',
                                        'placeholder' => 'Ukuran',
                                        'readonly',
                                    ]) !!}
                                    {!! Form::text('[]', 'Jumlah : ' . $item->stock, [
                                        'class' => 'form-control',
                                        'placeholder' => 'Jumlah',
                                        'readonly',
                                    ]) !!}
                                    {!! Form::text('[]', 'Harga : ' . $item->harga_beli, [
                                        'class' => 'form-control',
                                        'placeholder' => 'Harga Beli',
                                        'readonly',
                                    ]) !!}
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="card-footer">
                        {!! Form::submit('Validasi', ['class' => 'btn btn-primary']) !!}
                        <a href="{{ route('keuanganKeluars.index') }}" class="btn btn-default">Cancel</a>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
