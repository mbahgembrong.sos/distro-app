<div class="table-responsive">
    <table class="table" id="keranjangs-table">
        <thead>
        <tr>
            <th>Pelanggan Id</th>
        <th>Produk Id</th>
        <th>Detail Ukuran Produk Id</th>
        <th>Jumlah</th>
        <th>Total</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($keranjangs as $keranjang)
            <tr>
                <td>{{ $keranjang->pelanggan_id }}</td>
            <td>{{ $keranjang->produk_id }}</td>
            <td>{{ $keranjang->detail_ukuran_produk_id }}</td>
            <td>{{ $keranjang->jumlah }}</td>
            <td>{{ $keranjang->total }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['keranjangs.destroy', $keranjang->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('keranjangs.show', [$keranjang->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('keranjangs.edit', [$keranjang->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
