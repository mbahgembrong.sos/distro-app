<div class="table-responsive">
    <table class="table" id="detailBeliProduks-table">
        <thead>
        <tr>
            <th>Detail Ukuran Produk Id</th>
        <th>Stock</th>
        <th>Harga Beli</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($detailBeliProduks as $detailBeliProduk)
            <tr>
                <td>{{ $detailBeliProduk->detail_ukuran_produk_id }}</td>
            <td>{{ $detailBeliProduk->stock }}</td>
            <td>{{ $detailBeliProduk->harga_beli }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['detailBeliProduks.destroy', $detailBeliProduk->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('detailBeliProduks.show', [$detailBeliProduk->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('detailBeliProduks.edit', [$detailBeliProduk->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
