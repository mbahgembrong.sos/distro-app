<?php

namespace App\Http\Controllers;

use App\Models\Transaksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laracasts\Flash\Flash;

class JurnalHarianController extends Controller
{
    public function index()
    {
        $jurnalHarian = Transaksi::where('status', '=', 'selesai')->where('validasi',  0)
            ->select(DB::raw('sum(grand_total) as total'), DB::raw('count(*) as jumlah'), DB::raw('date(updated_at) as tanggal'), DB::raw('sum(validasi) as validasi'))->groupby('tanggal')->get();
        return view('jurnal_harian.index', compact('jurnalHarian'));
    }
    public function store(Request $request)
    {
        $jurnalHarian = Transaksi::where('status', '=', 'selesai')->where('validasi',  0)->whereDate('updated_at', '=', $request->tanggal)->get();
        foreach ($jurnalHarian as $jurnal) {
            $jurnal->validasi = 1;
            $jurnal->save();
        }
        Flash::success('Jurnal Harian updated successfully.');
        return view('jurnal_harian.index', compact('jurnalHarian'));
    }
    public function keuangan_masuk()
    {
        $months = [
            "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
        ];
        $jurnalHarian = Transaksi::where('status', '=', 'selesai')->where('validasi',  1)
            ->select(DB::raw('sum(grand_total) as total'), DB::raw('count(*) as jumlah'), DB::raw('date(updated_at) as tanggal'), DB::raw('sum(validasi) as validasi'))->groupby('tanggal')->get();
        $keuanganMasukPerMonth = [];
        for ($i = 0; $i < count($months); $i++) {
            $this->month = $i + 1;
            $perMonth =  Transaksi::where('status', '=', 'selesai')->where('validasi',  1)->WhereYear('updated_at', date('Y'))->WhereMonth('updated_at', $this->month)
                ->select(DB::raw('sum(grand_total) as total'), DB::raw('count(*) as jumlah'), DB::raw('date(updated_at) as tanggal'))->groupby('tanggal')->get()
                ->pluck('total')
                ->sum();
            array_push($keuanganMasukPerMonth, $perMonth);
        }
        $keuanganMasukPerMonth = json_encode($keuanganMasukPerMonth, JSON_PRETTY_PRINT);

        return view('jurnal_harian.keuangan_masuk', compact('keuanganMasukPerMonth', 'months'))
            ->with('jurnalHarian', $jurnalHarian);
    }
}
