<!-- Pelanggan Id Field -->
<div class="col-sm-12">
    {!! Form::label('pelanggan_id', 'Pelanggan Id:') !!}
    <p>{{ $keranjang->pelanggan_id }}</p>
</div>

<!-- Produk Id Field -->
<div class="col-sm-12">
    {!! Form::label('produk_id', 'Produk Id:') !!}
    <p>{{ $keranjang->produk_id }}</p>
</div>

<!-- Detail Ukuran Produk Id Field -->
<div class="col-sm-12">
    {!! Form::label('detail_ukuran_produk_id', 'Detail Ukuran Produk Id:') !!}
    <p>{{ $keranjang->detail_ukuran_produk_id }}</p>
</div>

<!-- Jumlah Field -->
<div class="col-sm-12">
    {!! Form::label('jumlah', 'Jumlah:') !!}
    <p>{{ $keranjang->jumlah }}</p>
</div>

<!-- Total Field -->
<div class="col-sm-12">
    {!! Form::label('total', 'Total:') !!}
    <p>{{ $keranjang->total }}</p>
</div>

