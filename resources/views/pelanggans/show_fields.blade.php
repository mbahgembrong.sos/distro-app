<!-- Nama Field -->
<div class="col-sm-12">
    {!! Form::label('nama', 'Nama:') !!}
    <p>{{ $pelanggan->nama }}</p>
</div>

<!-- Email Field -->
<div class="col-sm-12">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $pelanggan->email }}</p>
</div>

<!-- Password Field -->
<div class="col-sm-12">
    {!! Form::label('password', 'Password:') !!}
    <p>{{ $pelanggan->password }}</p>
</div>

<!-- Telp Field -->
<div class="col-sm-12">
    {!! Form::label('telp', 'Telp:') !!}
    <p>{{ $pelanggan->telp }}</p>
</div>

<!-- Alamat Field -->
<div class="col-sm-12">
    {!! Form::label('alamat', 'Alamat:') !!}
    <p>{{ $pelanggan->alamat }}</p>
</div>

<!-- Kodepos Field -->
<div class="col-sm-12">
    {!! Form::label('kodepos', 'Kodepos:') !!}
    <p>{{ $pelanggan->kodepos }}</p>
</div>

<!-- Kota Id Field -->
<div class="col-sm-12">
    {!! Form::label('kota_id', 'Kota Id:') !!}
    <p>{{ $pelanggan->kota_id }}</p>
</div>

<!-- Foto Field -->
<div class="col-sm-12">
    {!! Form::label('foto', 'Foto:') !!}
    <p>{{ $pelanggan->foto }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $pelanggan->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $pelanggan->updated_at }}</p>
</div>

