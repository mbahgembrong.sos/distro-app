<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Keranjang
 * @package App\Models
 * @version July 21, 2022, 4:06 pm UTC
 *
 * @property unsignedInteger $pelanggan_id
 * @property unsignedInteger $produk_id
 * @property unsignedInteger $detail_ukuran_produk_id
 * @property integer $jumlah
 * @property integer $total
 */
class Keranjang extends Model
{

    use HasFactory;

    public $table = 'keranjangs';




    public $fillable = [
        'pelanggan_id',
        'produk_id',
        'detail_ukuran_produk_id',
        'jumlah',
        'total'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'jumlah' => 'integer',
        'total' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'pelanggan_id' => 'required|integer',
        'produk_id' => 'required',
        'detail_ukuran_produk_id' => 'required',
        'jumlah' => 'required|integer'
    ];
    /**
     * Get the pelanggan that owns the Keranjang
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function pelanggan(): BelongsTo
    {
        return $this->belongsTo(pelanggan::class,'pelanggan_id');
    }
    /**
     * Get the produk that owns the Keranjang
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function produk(): BelongsTo
    {
        return $this->belongsTo(Produk::class,'produk_id','id');
    }
    /**
     * Get the detailUkuranProduk that owns the Keranjang
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function detailUkuranProduk(): BelongsTo
    {
        return $this->belongsTo(DetailUkuranProduk::class,'detail_ukuran_produk_id');
    }
}
