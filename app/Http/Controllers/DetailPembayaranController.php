<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDetailPembayaranRequest;
use App\Http\Requests\UpdateDetailPembayaranRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\DetailPembayaran;
use App\Models\Transaksi;
use Illuminate\Http\Request;
use Flash;
use Response;

class DetailPembayaranController extends AppBaseController
{
    /**
     * Display a listing of the DetailPembayaran.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var DetailPembayaran $detailPembayarans */
        $detailPembayarans = DetailPembayaran::all();

        return view('detail_pembayarans.index')
            ->with('detailPembayarans', $detailPembayarans);
    }

    /**
     * Show the form for creating a new DetailPembayaran.
     *
     * @return Response
     */
    public function create()
    {
        $transaksis = Transaksi::pluck('id', 'id');
        return view('detail_pembayarans.create',compact('transaksis'));
    }

    /**
     * Store a newly created DetailPembayaran in storage.
     *
     * @param CreateDetailPembayaranRequest $request
     *
     * @return Response
     */
    public function store(CreateDetailPembayaranRequest $request)
    {
        $input = $request->all();

        /** @var DetailPembayaran $detailPembayaran */
        $detailPembayaran = DetailPembayaran::create($input);

        Flash::success('Detail Pembayaran saved successfully.');

        return redirect(route('detailPembayarans.index'));
    }

    /**
     * Display the specified DetailPembayaran.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var DetailPembayaran $detailPembayaran */
        $detailPembayaran = DetailPembayaran::find($id);

        if (empty($detailPembayaran)) {
            Flash::error('Detail Pembayaran not found');

            return redirect(route('detailPembayarans.index'));
        }

        return view('detail_pembayarans.show')->with('detailPembayaran', $detailPembayaran);
    }

    /**
     * Show the form for editing the specified DetailPembayaran.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var DetailPembayaran $detailPembayaran */
        $detailPembayaran = DetailPembayaran::find($id);

        if (empty($detailPembayaran)) {
            Flash::error('Detail Pembayaran not found');

            return redirect(route('detailPembayarans.index'));
        }
        $transaksis = Transaksi::pluck('id', 'id');

        return view('detail_pembayarans.edit',compact('transaksis'))->with('detailPembayaran', $detailPembayaran);
    }

    /**
     * Update the specified DetailPembayaran in storage.
     *
     * @param int $id
     * @param UpdateDetailPembayaranRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDetailPembayaranRequest $request)
    {
        /** @var DetailPembayaran $detailPembayaran */
        $detailPembayaran = DetailPembayaran::find($id);

        if (empty($detailPembayaran)) {
            Flash::error('Detail Pembayaran not found');

            return redirect(route('detailPembayarans.index'));
        }

        $detailPembayaran->fill($request->all());
        $detailPembayaran->save();

        Flash::success('Detail Pembayaran updated successfully.');

        return redirect(route('detailPembayarans.index'));
    }

    /**
     * Remove the specified DetailPembayaran from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var DetailPembayaran $detailPembayaran */
        $detailPembayaran = DetailPembayaran::find($id);

        if (empty($detailPembayaran)) {
            Flash::error('Detail Pembayaran not found');

            return redirect(route('detailPembayarans.index'));
        }

        $detailPembayaran->delete();

        Flash::success('Detail Pembayaran deleted successfully.');

        return redirect(route('detailPembayarans.index'));
    }
}
