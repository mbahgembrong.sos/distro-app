<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new \App\Models\Role();
        $role->nama = 'Kasir';
        $role->save();
        $role = new \App\Models\Role();
        $role->nama = 'Admin Gudang';
        $role->save();
        $role = new \App\Models\Role();
        $role->nama = 'Keuangan';
        $role->save();
        $role = new \App\Models\Role();
        $role->nama = 'Pemilik';
        $role->save();
    }
}
