<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class DetailUkuranProduk
 * @package App\Models
 * @version July 21, 2022, 2:05 pm UTC
 *
 */
class DetailUkuranProduk extends Model
{

    use HasFactory;

    public $table = 'detail_ukuran_produks';




    public $fillable = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'ukuran' => 'string',
        'stock' => 'integer',
        'harga' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'produk_id' => 'required',
        'ukuran' => 'required',
        'harga' => 'nullable'
    ];

    public function getHargaNominal()
    {
        return 'Rp. ' . number_format(($this->harga * $this->detailBeliProduk()->orderBy('updated_at', 'desc')->first()->harga_beli / 100) + $this->detailBeliProduk()->orderBy('updated_at', 'desc')->first()->harga_beli, 0, ',', '.');
    }
    /**
     * Get the produk that owns the DetailUkuranProduk
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_id');
    }
    /**
     * Get all of the detailTransaksi for the DetailUkuranProduk
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function detailTransaksi()
    {
        return $this->hasMany(DetailTransaksi::class, 'detail_ukuran_produk_id', 'id');
    }
    public function detailBeliProduk()
    {
        return $this->hasMany(DetailBeliProduk::class, 'detail_ukuran_produk_id', 'id');
    }
    /**
     * Get all of the keranjang for the DetailUkuranProduk
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function keranjang()
    {
        return $this->hasMany(Keranjang::class, 'detail_ukuran_produk_id', 'id');
    }
}
