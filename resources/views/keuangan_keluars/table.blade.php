<div class="table-responsive">
    <table class="table" id="keuanganKeluars-table">
        <thead>
            <tr>
                <th>Tanggal</th>
                <th>Nama</th>
                <th>Jenis</th>
                <th>Keterangan</th>
                <th>Total</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($keuanganKeluars as $keuanganKeluar)
                <tr>
                    <td>{{ $keuanganKeluar->created_at->format('d/m/Y') }}</td>
                    <td>{{ $keuanganKeluar->nama }}</td>
                    <td>{{ $keuanganKeluar->jenis }}</td>
                    <td>{{ $keuanganKeluar->keterangan ?? ($keuanganKeluar->validasi ? 'sudah validasi' : 'belum validasi') }}
                    </td>
                    <td>{{ $keuanganKeluar->total }}</td>
                    <td width="120">
                        @if ($keuanganKeluar->validasi == 0)
                            {!! Form::open(['route' => ['keuanganKeluars.destroy', $keuanganKeluar->id], 'method' => 'delete']) !!}
                            <div class='btn-group'>
                                @if ($keuanganKeluar->jenis == 'pembelian produk')
                                    <a href="{{ route('keuanganKeluars.show', [$keuanganKeluar->id]) }}"
                                        class='btn btn-default btn-xs'>
                                        <i class="far fa-eye"></i>
                                    </a>
                                @else
                                    <a href="{{ route('keuanganKeluars.edit', [$keuanganKeluar->id]) }}"
                                        class='btn btn-default btn-xs'>
                                        <i class="far fa-edit"></i>
                                    </a>
                                    {!! Form::button('<i class="far fa-trash-alt"></i>', [
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'onclick' => "return confirm('Are you sure?')",
                                    ]) !!}
                                @endif
                            </div>
                            {!! Form::close() !!}
                        @endif

                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
