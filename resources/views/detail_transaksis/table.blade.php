<div class="table-responsive">
    <table class="table" id="detailTransaksis-table">
        <thead>
        <tr>
            
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($detailTransaksis as $detailTransaksi)
            <tr>
                
                <td width="120">
                    {!! Form::open(['route' => ['detailTransaksis.destroy', $detailTransaksi->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('detailTransaksis.show', [$detailTransaksi->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('detailTransaksis.edit', [$detailTransaksi->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
