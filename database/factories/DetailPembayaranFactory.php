<?php

namespace Database\Factories;

use App\Models\DetailPembayaran;
use Illuminate\Database\Eloquent\Factories\Factory;

class DetailPembayaranFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DetailPembayaran::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'transaksi_id' => $this->faker->word,
        'nama' => $this->faker->word,
        'bank' => $this->faker->word,
        'jumlah' => $this->faker->randomDigitNotNull,
        'tanggal' => $this->faker->word,
        'bukti' => $this->faker->word,
        'validasi' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
