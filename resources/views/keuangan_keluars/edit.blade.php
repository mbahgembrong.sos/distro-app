@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1>Add Bukti {{ $keuanganKeluar->nama }}</h1>
                </div>
            </div>
        </div>
    </section>

    <div class="content px-3">

        @include('adminlte-templates::common.errors')

        <div class="card">

            {!! Form::model($keuanganKeluar, [
                'route' => ['keuanganKeluars.update', $keuanganKeluar->id],
                'method' => 'patch',
                'files' => true,
            ]) !!}

            <div class="card-body">
                <div class="row">
                    <!-- Jenis Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('nama', 'nama :') !!}
                        {!! Form::text('nama', $keuanganKeluar->nama, ['class' => 'form-control', 'disabled']) !!}
                    </div>
                    <!-- Total Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('total', 'Total:') !!}
                        {!! Form::number('total', $keuanganKeluar->total, ['class' => 'form-control', 'readonly']) !!}
                    </div>
                    <!-- Total Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('jenis', 'Total:') !!}
                        {!! Form::text('jenis', $keuanganKeluar->jenis, ['class' => 'form-control', 'readonly']) !!}
                    </div>
                    <input type="hidden" name="validasi" value="true">

                    <!-- Foto Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::label('foto', 'Foto:') !!}
                        <div class="input-group">
                            <div class="custom-file">
                                {!! Form::label('foto', 'Choose file', ['class' => 'custom-file-label']) !!}
                                {!! Form::file('foto', ['class' => 'custom-file-input']) !!}
                            </div>
                        </div>
                    </div>

                    <!-- Keterangan Field -->
                    <div class="form-group col-sm-12 col-lg-12">
                        {!! Form::label('keterangan', 'Keterangan:') !!}
                        {!! Form::textarea('keterangan', $keuanganKeluar->keterangan, ['class' => 'form-control']) !!}
                    </div>


                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="card-footer">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('keuanganKeluars.index') }}" class="btn btn-default">Cancel</a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection
