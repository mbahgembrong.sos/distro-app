<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

/**
 * Class Pelanggan
 * @package App\Models
 * @version July 21, 2022, 2:19 pm UTC
 *
 * @property string $nama
 * @property string $email
 * @property string $password
 * @property string $telp
 * @property string $alamat
 * @property integer $kodepos
 * @property integer $kota_id
 * @property string $foto
 */
class Pelanggan extends Authenticatable
{

    use HasFactory;
    use Notifiable, HasApiTokens;

    public $table = 'pelanggans';
    protected $guard = 'web';

    public $fillable = [
        'nama',
        'email',
        'password',
        'telp',
        'alamat',
        'kodepos',
        'kota_id',
        'foto', 'email_verfied_at'
    ];
    protected $hidden = ['password'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nama' => 'string',
        'email' => 'string',
        'password' => 'string',
        'telp' => 'string',
        'alamat' => 'string',
        'kodepos' => 'integer',
        'kota_id' => 'integer',
        'foto' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nama' => 'required|string',
        'email' => 'required|unique:pelanggans,email',
        'password' => 'required|string',
        'telp' => 'required|unique:pelanggans,telp',
        'alamat' => 'required|string',
        'kodepos' => 'numeric|nullable',
        'kota_id' => 'nullable|numeric',
        'foto' => 'nullable|file'
    ];

    /**
     * Get all of the transaksi for the Pelanggan
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transaksi(): HasMany
    {
        return $this->hasMany(Transaksi::class, 'pelanggan_id', 'id');
    }
    /**
     * Get all of the keranjang for the Pelanggan
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function keranjang(): HasMany
    {
        return $this->hasMany(Keranjang::class, 'pelanggan_id', 'id');
    }
}
