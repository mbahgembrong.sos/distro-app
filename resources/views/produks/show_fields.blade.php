<!-- Kategori Id Field -->
<div class="col-sm-6">
    {!! Form::label('kategori', 'Kategori:') !!}
    <p>{{ $produk->kategori->nama }}</p>
</div>

<!-- Nama Field -->
<div class="col-sm-6">
    {!! Form::label('nama', 'Nama:') !!}
    <p>{{ $produk->nama }}</p>
</div>

<!-- Berat Field -->
<div class="col-sm-6">
    {!! Form::label('berat', 'Berat:') !!}
    <p>{{ $produk->berat }}</p>
</div>

<!-- Foto Field -->
<div class="col-sm-6">
    {!! Form::label('foto', 'Foto:') !!}
    <p>{{ $produk->foto }}</p>
</div>

<!-- Deskripsi Field -->
<div class="col-sm-12">
    {!! Form::label('deskripsi', 'Deskripsi:') !!}
    <p>{{ $produk->deskripsi }}</p>
</div>
