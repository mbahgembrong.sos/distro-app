@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1>Edit Produk</h1>
                </div>
            </div>
        </div>
    </section>

    <div class="content px-3">

        @include('adminlte-templates::common.errors')

        <div class="card">

            {!! Form::model($produk, ['route' => ['produks.update', $produk->id], 'method' => 'patch', 'files' => true]) !!}

            <div class="card-body">
                <div class="row">
                    @include('produks.fields')
                    @php
                        $no = 0;
                    @endphp
                    @foreach ($produk->detailUkuranProduk as $item)
                        <div class="form-group col-sm-12" id="transactions">
                            {!! Form::label('ukuran', 'Ukuran :') !!}
                            <div class="row">
                                @php
                                    $no++;
                                @endphp
                                @if ($no == 1)
                                    <div class="form-group fieldGroup col-6" data-id="1">
                                        <div class="input-group">
                                            {!! Form::text('ukuran[]', $item->ukuran, ['class' => 'form-control', 'placeholder' => 'Ukuran', ]) !!}
                                            <div class="input-group-addon ml-3">
                                                <a href="javascript:void(0)" class="btn btn-success addMore"><i
                                                        class="fas fa-plus"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="form-group fieldGroupCopy col-6" style="display: none;">
                                        <div class="input-group">
                                            {!! Form::text('ukuran[]', $item->ukuran, ['class' => 'form-control', 'placeholder' => 'Ukuran', ]) !!}
                                            <div class="input-group-addon ml-3">
                                                <a href="javascript:void(0)" class="btn btn-danger remove"><i
                                                        class="fas fa-trash"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    @endforeach
                    {{-- trash --}}
                    <div class="form-group fieldGroupCopy col-6" style="display: none;">
                        <div class="input-group">
                            {!! Form::text('ukuran[]', null, ['class' => 'form-control', 'placeholder' => 'Ukuran']) !!}
                            <div class="input-group-addon ml-3">
                                <a href="javascript:void(0)" class="btn btn-danger remove"><i class="fas fa-trash"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-footer">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('produks.index') }}" class="btn btn-default">Cancel</a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection
@push('page_scripts')
    <script>
        $(document).ready(function() {
            // membatasi jumlah inputan
            var maxGroup = 10;

            //melakukan proses multiple input
            $(".addMore").click(function() {
                var layanansInput = $('body').find('.fieldGroup').length;
                if (layanansInput < maxGroup) {
                    var fieldHTML = '<div class="form-group fieldGroup col-6" data-id="' + (layanansInput +
                            1) +
                        '">' + $(".fieldGroupCopy").html() + '</div>';
                    $('body').find('.fieldGroup:last').after(fieldHTML);
                } else {
                    alert('Maksimal ' + maxGroup + ' Layanan.');
                }
            });
            //remove fields group
            $("body").on("click", ".remove", function() {
                $(this).parents(".fieldGroup").remove();
            });
        });
    </script>
@endpush
