<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransaksisTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksis', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('pelanggan_id');
            $table->unsignedInteger('kasir_id')->nullable();
            $table->string('status');
            $table->integer('grand_total');
            $table->integer('berat_total');
            $table->string('expedisi')->nullable();
            $table->integer('diskon')->nullable();
            $table->string('type_expedisi')->nullable();
            $table->integer('ongkir')->nullable();
            $table->integer('estimasi')->nullable();
            $table->string('resi_pengiriman')->nullable();
            $table->string('type_pembelian')->nullable();
            $table->timestamps();
        });
        Schema::table('transaksis', function (Blueprint  $table) {
            $table->foreign('pelanggan_id')->references('id')->on('pelanggans')->onDelete('cascade');
            $table->foreign('kasir_id')->references('id')->on('karyawans')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transaksis');
    }
}
