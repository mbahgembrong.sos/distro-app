<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateKeuanganKeluarRequest;
use App\Http\Requests\UpdateKeuanganKeluarRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Karyawan;
use App\Models\KeuanganKeluar;
use App\Models\Tagihanan;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Storage;
use Response;

class KeuanganKeluarController extends AppBaseController
{
    public function __construct()
    {
        $this->jenis =
            ['tagihan' => 'tagihan',  'gaji karyawan' => 'gaji karyawan', 'lain lain' => 'lain lain'];
    }
    /**
     * Display a listing of the KeuanganKeluar.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var KeuanganKeluar $keuanganKeluars */
        $keuanganKeluars = KeuanganKeluar::all();
        $months = [
            "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
        ];
        $keuanganKeluarPerMonth = [];
        for ($i = 0; $i < count($months); $i++) {
            $this->month = $i + 1;
            $perMonth = KeuanganKeluar::WhereYear('updated_at', date('Y'))->WhereMonth('updated_at', $this->month)
                ->where('validasi', 1)
                ->pluck('total')
                ->sum();
            array_push($keuanganKeluarPerMonth, $perMonth);
        }
        $keuanganKeluarPerMonth = json_encode($keuanganKeluarPerMonth, JSON_PRETTY_PRINT);

        return view('keuangan_keluars.index', compact('keuanganKeluarPerMonth', 'months'))
            ->with('keuanganKeluars', $keuanganKeluars);
    }

    /**
     * Show the form for creating a new KeuanganKeluar.
     *
     * @return Response
     */
    public function create()
    {
        $tagihans = Tagihanan::all();
        $karyawans = Karyawan::all();
        $jenisKeuangan = $this->jenis;
        return view('keuangan_keluars.create', compact(['tagihans', 'karyawans', 'jenisKeuangan']));
    }

    /**
     * Store a newly created KeuanganKeluar in storage.
     *
     * @param CreateKeuanganKeluarRequest $request
     *
     * @return Response
     */
    public function store(CreateKeuanganKeluarRequest $request)
    {
        Storage::disk('public')->put('keuangan-keluar/' . $request->foto->getClientOriginalName(), file_get_contents($request->foto));
        // dd($request->all());
        $input = $request->all();
        if ($request->jenis == 'tagihan') {
            $input['foregin_id'] = $request->tagihan;
        }
        if ($request->jenis == 'gaji karyawan') {
            $input['foregin_id'] = $request->karyawan;
        }
        $input['validasi'] = 1;
        $input['foto'] = $request->foto->getClientOriginalName();
        $kategori = KeuanganKeluar::create($input);

        Flash::success('Keuangan Keluar saved successfully.');

        return redirect(route('keuanganKeluars.index'));
    }

    /**
     * Display the specified KeuanganKeluar.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var KeuanganKeluar $keuanganKeluar */
        $keuanganKeluar = KeuanganKeluar::find($id);

        if (empty($keuanganKeluar)) {
            Flash::error('Keuangan Keluar not found');

            return redirect(route('keuanganKeluars.index'));
        }

        return view('keuangan_keluars.show')->with('keuanganKeluar', $keuanganKeluar);
    }
    public function validasi($id)
    {
        $keuanganKeluar = KeuanganKeluar::find($id);
        if (empty($keuanganKeluar)) {
            Flash::error('Keuangan Keluar not found');

            return redirect(route('keuanganKeluars.index'));
        }

        $keuanganKeluar->validasi = true;
        $keuanganKeluar->save();

        Flash::success('Keuangan Keluar updated validasi successfully.');

        return redirect(route('keuanganKeluars.index'));
    }

    /**
     * Show the form for editing the specified KeuanganKeluar.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var KeuanganKeluar $keuanganKeluar */
        $keuanganKeluar = KeuanganKeluar::find($id);

        if (empty($keuanganKeluar)) {
            Flash::error('Keuangan Keluar not found');

            return redirect(route('keuanganKeluars.index'));
        }

        return view('keuangan_keluars.edit')->with('keuanganKeluar', $keuanganKeluar);
    }

    /**
     * Update the specified KeuanganKeluar in storage.
     *
     * @param int $id
     * @param UpdateKeuanganKeluarRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateKeuanganKeluarRequest $request)
    {
        /** @var KeuanganKeluar $keuanganKeluar */
        $keuanganKeluar = KeuanganKeluar::find($id);

        if (empty($keuanganKeluar)) {
            Flash::error('Keuangan Keluar not found');

            return redirect(route('keuanganKeluars.index'));
        }
        Storage::disk('public')->put('keuangan-keluar/' . $request->foto->getClientOriginalName(), file_get_contents($request->foto));
        $keuanganKeluar->fill($request->all());
        $keuanganKeluar->foto = $request->foto->getClientOriginalName();
        $keuanganKeluar->validasi = true;
        $keuanganKeluar->save();

        Flash::success('Keuangan Keluar updated successfully.');

        return redirect(route('keuanganKeluars.index'));
    }

    /**
     * Remove the specified KeuanganKeluar from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var KeuanganKeluar $keuanganKeluar */
        $keuanganKeluar = KeuanganKeluar::find($id);

        if (empty($keuanganKeluar)) {
            Flash::error('Keuangan Keluar not found');

            return redirect(route('keuanganKeluars.index'));
        }

        $keuanganKeluar->delete();

        Flash::success('Keuangan Keluar deleted successfully.');

        return redirect(route('keuanganKeluars.index'));
    }
}
