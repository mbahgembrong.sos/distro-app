<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Role
 * @package App\Models
 * @version July 21, 2022, 2:07 pm UTC
 *
 * @property string $nama
 */
class Role extends Model
{

    use HasFactory;

    public $table = 'roles';




    public $fillable = [
        'nama'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nama' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nama' => 'required:string'
    ];
    /**
     * Get all of the karyawan for the Role
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function karyawan(): HasMany
    {
        return $this->hasMany(Karyawan::class, 'role_id', 'id');
    }
}
