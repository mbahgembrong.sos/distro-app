<?php

namespace App\Console\Commands;

use App\Models\Karyawan;
use App\Models\KeuanganKeluar;
use App\Models\Tagihanan;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class GajiKaryawanCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:karyawan';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $karyawans = Karyawan::all();
        foreach ($karyawans as $karyawan) {
            $bayarGaji = KeuanganKeluar::where('foregin_id', $karyawan->id)->where('jenis', 'gaji karyawan')->whereDate('created_at', '>=', env('SCEDJULE_TAHUN_BULAN', date('Y-m')) . '-01')->whereDate('created_at', '<=', env('SCEDJULE_TAHUN_BULAN_TANGGAL', date('Y-m-d')))->first();
            if ($karyawan->scedjule == env('TAGIHAN_BULAN_TANGGAL',  date('m-d')) && $bayarGaji == null) {
                $keuanganKeluar = new KeuanganKeluar();
                $keuanganKeluar->nama = "Bayar Gaji Bulan " . date('m') . ' ' . $karyawan->nama;
                $keuanganKeluar->jenis = "gaji karyawan";
                $keuanganKeluar->total = $karyawan->gaji;
                $keuanganKeluar->foto = null;
                $keuanganKeluar->keterangan = 'belum bayar';
                $keuanganKeluar->foregin_id = $karyawan->id;
                $keuanganKeluar->save();
                Log::info('Bayar Gaji Karyawan Bulan ' . date('m') . ' ' . $karyawan->nama . ' telah ditambahkan');
            }
        }

        Log::info('Tidak ada gaji karyawan yang ditagihkan');
    }
}
