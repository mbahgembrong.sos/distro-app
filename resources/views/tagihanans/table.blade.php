<div class="table-responsive">
    <table class="table" id="tagihanans-table">
        <thead>
            <tr>
                <th>Nama</th>
                <th>Jenis Tagihan</th>
                <th>Jumlah</th>
                <th>Scedjule</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($tagihanans as $tagihanan)
                <tr>
                    <td>{{ $tagihanan->nama }}</td>
                    <td>{{ $tagihanan->jenis_tagihan }}</td>
                    <td>{{ $tagihanan->jumlah }}</td>
                    <td>{{ $tagihanan->jenis_tagihan == 'Tahun' ? date_format(date_create('2022-' . $tagihanan->scedjule . '-01'), 'F') : date_format(date_create('2022-' . $tagihanan->scedjule), 'F-d') }}
                    </td>
                    <td width="120">
                        {!! Form::open(['route' => ['tagihanans.destroy', $tagihanan->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{{ route('tagihanans.show', [$tagihanan->id]) }}" class='btn btn-default btn-xs'>
                                <i class="far fa-eye"></i>
                            </a>
                            <a href="{{ route('tagihanans.edit', [$tagihanan->id]) }}" class='btn btn-default btn-xs'>
                                <i class="far fa-edit"></i>
                            </a>
                            {!! Form::button('<i class="far fa-trash-alt"></i>', [
                                'type' => 'submit',
                                'class' => 'btn btn-danger btn-xs',
                                'onclick' => "return confirm('Are you sure?')",
                            ]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
