<!-- Pelanggan Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pelanggan_id', 'Pelanggan Id:') !!}
    {!! Form::select('pelanggan_id', $pelanggans, null, ['class' => 'form-control custom-select']) !!}
</div>


<!-- Produk Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('produk_id', 'Produk Id:') !!}
    {!! Form::select('produk_id', $produks, null, ['class' => 'form-control custom-select']) !!}
</div>


<!-- Detail Ukuran Produk Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('detail_ukuran_produk_id', 'Detail Ukuran Produk Id:') !!}
    {!! Form::select('detail_ukuran_produk_id', $detail_ukuran_produks, null, ['class' => 'form-control custom-select']) !!}
</div>


<!-- Jumlah Field -->
<div class="form-group col-sm-6">
    {!! Form::label('jumlah', 'Jumlah:') !!}
    {!! Form::number('jumlah', null, ['class' => 'form-control']) !!}
</div>

<!-- Total Field -->
<div class="form-group col-sm-6">
    {!! Form::label('total', 'Total:') !!}
    {!! Form::text('total', null, ['class' => 'form-control']) !!}
</div>
