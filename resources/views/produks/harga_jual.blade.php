@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Harga Jual Produk</h1>
                </div>
                <div class="col-sm-6">
                    <a class="btn btn-default float-right" href="{{ route('produks.index') }}">
                        Back
                    </a>
                </div>
            </div>
        </div>
    </section>
    <div class="content px-3">
        <div class="card">
            {!! Form::open(['route' => ['produks.harga_jual.update', $produk->id], 'files' => true]) !!}
            <div class="card-body">
                <div class="row">
                    @include('produks.show_fields')
                    <div class="form-group col-sm-12" id="transactions">
                        {!! Form::label('ukuran', 'Update Harga Jual :') !!}
                        <div class="row">
                            @foreach ($produk->detailUkuranProduk as $item)
                                <div class="form-group fieldGroup col-12" data-id="1">
                                    <div class="input-group">
                                        <input type="hidden" name="detail_ukuran_produk_id[]" value="{{ $item->id }}">
                                        {!! Form::text('', 'Ukuran : ' . $item->ukuran, [
                                            'class' => 'form-control',
                                            'placeholder' => 'Ukuran',
                                            'readonly',
                                        ]) !!}
                                        {!! Form::text(
                                            '',
                                            ' Harga Beli : ' .
                                                $item->detailBeliProduk()->orderBy('updated_at', 'desc')->first()->harga_beli,
                                            ['class' => 'form-control', 'placeholder' => 'Stock', 'readonly'],
                                        ) !!}
                                        {!! Form::number('harga[]', $item->harga, [
                                            'class' => 'form-control',
                                            'placeholder' => 'Harga Jual',
                                        ]) !!}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="clearfix"></div>

                </div>
            </div>
            <div class="card-footer">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('produks.index') }}" class="btn btn-default">Cancel</a>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
@endsection
@push('page_scripts')
    @if (auth()->guard('karyawan')->user()->role()->first()->nama == 'Pemilik')
        <Script>
            (window).ready(function() {
                $('input[type="submit"]').value('Validasi').text('Validasi');
            })
        </Script>
    @endif
@endpush
