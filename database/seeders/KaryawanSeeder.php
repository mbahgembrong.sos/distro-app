<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class KaryawanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::where('nama', 'Kasir')->first();
        $karyawan = new \App\Models\Karyawan();
        $karyawan->role_id = $role->id;
        $karyawan->nama = 'Kasir';
        $karyawan->email = 'kasir@gmail.com';
        $password = bcrypt('kasir');
        $karyawan->password = $password;
        $karyawan->telp = '081234567890';
        $karyawan->alamat = 'Jl. Kebon Jeruk No. 1';
        $karyawan->gaji = '50000';
        $karyawan->scedjule = '01';
        $karyawan->save();
        $role = Role::where('nama', 'Keuangan')->first();
        $karyawan = new \App\Models\Karyawan();
        $karyawan->role_id = $role->id;
        $karyawan->nama = 'Keuangan';
        $karyawan->email = 'keuangan@gmail.com';
        $password = bcrypt('keuangan');
        $karyawan->password = $password;
        $karyawan->telp = '081234567891';
        $karyawan->alamat = 'Jl. Kebon Jeruk No. 1';
        $karyawan->gaji = '50000';
        $karyawan->scedjule = '01';
        $karyawan->save();
        $role = Role::where('nama', 'Admin Gudang')->first();
        $karyawan = new \App\Models\Karyawan();
        $karyawan->role_id = $role->id;
        $karyawan->nama = 'Admin Gudang';
        $karyawan->email = 'admingudang@gmail.com';
        $password = bcrypt('admingudang');
        $karyawan->password = $password;
        $karyawan->telp = '081234567892';
        $karyawan->alamat = 'Jl. Kebon Jeruk No. 1';
        $karyawan->gaji = '50000';
        $karyawan->scedjule = '01';
        $karyawan->save();
        $role = Role::where('nama', 'Pemilik')->first();
        $karyawan = new \App\Models\Karyawan();
        $karyawan->role_id = $role->id;
        $karyawan->nama = 'Pemilik';
        $karyawan->email = 'pemilik@gmail.com';
        $password = bcrypt('pemilik');
        $karyawan->password = $password;
        $karyawan->telp = '081234567890';
        $karyawan->alamat = 'Jl. Kebon Jeruk No. 1';
        $karyawan->gaji = '50000';
        $karyawan->scedjule = '01';
        $karyawan->save();
    }
}
