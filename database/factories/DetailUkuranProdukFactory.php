<?php

namespace Database\Factories;

use App\Models\DetailUkuranProduk;
use Illuminate\Database\Eloquent\Factories\Factory;

class DetailUkuranProdukFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DetailUkuranProduk::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'produk_id' => $this->faker->word,
        'ukuran' => $this->faker->word,
        'stock' => $this->faker->randomDigitNotNull,
        'harga' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
