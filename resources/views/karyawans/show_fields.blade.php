<!-- Role Id Field -->

<div class="col-sm-6">
    {!! Form::label('role_id', 'Role Id:') !!}
    <p>{{ $karyawan->role_id }}</p>
</div>

<!-- Nama Field -->
<div class="col-sm-6">
    {!! Form::label('nama', 'Nama:') !!}
    <p>{{ $karyawan->nama }}</p>
</div>

<!-- Email Field -->
<div class="col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $karyawan->email }}</p>
</div>


<!-- Telp Field -->
<div class="col-sm-6">
    {!! Form::label('telp', 'Telp:') !!}
    <p>{{ $karyawan->telp }}</p>
</div>

<!-- Alamat Field -->
<div class="col-sm-12">
    {!! Form::label('alamat', 'Alamat:') !!}
    <p>{{ $karyawan->alamat }}</p>
</div>

<!-- Gaji Field -->
<div class="col-sm-6">
    {!! Form::label('gaji', 'Gaji:') !!}
    <p>{{ $karyawan->gaji }}</p>
</div>

<!-- Scedjule Field -->
<div class="col-sm-6">
    {!! Form::label('scedjule', 'Scedjule:') !!}
    <p>{{ $karyawan->scedjule }}</p>
</div>

<!-- Foto Field -->
<div class="col-sm-6">
    {!! Form::label('foto', 'Foto:') !!}
    <p>{{ $karyawan->foto }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-6">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $karyawan->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-6">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $karyawan->updated_at }}</p>
</div>

