<div class="table-responsive">
    <table class="table" id="jurnal_harian-table">
        <thead>
            <tr>
                <th>Tanggal</th>
                <th>Jumlah</th>
                <th>Grand Total</th>
                {{-- @if ($jurnalHarian[0]->validasi == 0) --}}
                    <th colspan="3">Action</th>
                {{-- @endif --}}
            </tr>
        </thead>
        <tbody>
            @foreach ($jurnalHarian as $jurnal)
                <tr>
                    <td>{{ $jurnal->tanggal }}</td>
                    <td>{{ $jurnal->jumlah }} transaksi</td>
                    <td>Rp. {{ $jurnal->total }}</td>
                    @if ($jurnal->validasi == 0)
                        <td width="120">
                            {!! Form::open(['route' => ['jurnal_harian.store'], 'method' => 'post']) !!}
                            <input type="hidden" name="tanggal" value="{{ $jurnal->tanggal }}">
                            {!! Form::button('<i class="far fa-eye"></i>', [
                                'type' => 'submit',
                                'class' => 'btn btn-danger btn-xs',
                                'onclick' => "return confirm('Are you sure?')",
                            ]) !!}
                            {!! Form::close() !!}
                        </td>
                    @endif

                </tr>
            @endforeach
        </tbody>
    </table>
</div>
