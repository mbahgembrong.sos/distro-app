<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDetailTransaksiRequest;
use App\Http\Requests\UpdateDetailTransaksiRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\DetailTransaksi;
use Illuminate\Http\Request;
use Flash;
use Response;

class DetailTransaksiController extends AppBaseController
{
    /**
     * Display a listing of the DetailTransaksi.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var DetailTransaksi $detailTransaksis */
        $detailTransaksis = DetailTransaksi::all();

        return view('detail_transaksis.index')
            ->with('detailTransaksis', $detailTransaksis);
    }

    /**
     * Show the form for creating a new DetailTransaksi.
     *
     * @return Response
     */
    public function create()
    {
        return view('detail_transaksis.create');
    }

    /**
     * Store a newly created DetailTransaksi in storage.
     *
     * @param CreateDetailTransaksiRequest $request
     *
     * @return Response
     */
    public function store(CreateDetailTransaksiRequest $request)
    {
        $input = $request->all();

        /** @var DetailTransaksi $detailTransaksi */
        $detailTransaksi = DetailTransaksi::create($input);

        Flash::success('Detail Transaksi saved successfully.');

        return redirect(route('detailTransaksis.index'));
    }

    /**
     * Display the specified DetailTransaksi.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var DetailTransaksi $detailTransaksi */
        $detailTransaksi = DetailTransaksi::find($id);

        if (empty($detailTransaksi)) {
            Flash::error('Detail Transaksi not found');

            return redirect(route('detailTransaksis.index'));
        }

        return view('detail_transaksis.show')->with('detailTransaksi', $detailTransaksi);
    }

    /**
     * Show the form for editing the specified DetailTransaksi.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var DetailTransaksi $detailTransaksi */
        $detailTransaksi = DetailTransaksi::find($id);

        if (empty($detailTransaksi)) {
            Flash::error('Detail Transaksi not found');

            return redirect(route('detailTransaksis.index'));
        }

        return view('detail_transaksis.edit')->with('detailTransaksi', $detailTransaksi);
    }

    /**
     * Update the specified DetailTransaksi in storage.
     *
     * @param int $id
     * @param UpdateDetailTransaksiRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDetailTransaksiRequest $request)
    {
        /** @var DetailTransaksi $detailTransaksi */
        $detailTransaksi = DetailTransaksi::find($id);

        if (empty($detailTransaksi)) {
            Flash::error('Detail Transaksi not found');

            return redirect(route('detailTransaksis.index'));
        }

        $detailTransaksi->fill($request->all());
        $detailTransaksi->save();

        Flash::success('Detail Transaksi updated successfully.');

        return redirect(route('detailTransaksis.index'));
    }

    /**
     * Remove the specified DetailTransaksi from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var DetailTransaksi $detailTransaksi */
        $detailTransaksi = DetailTransaksi::find($id);

        if (empty($detailTransaksi)) {
            Flash::error('Detail Transaksi not found');

            return redirect(route('detailTransaksis.index'));
        }

        $detailTransaksi->delete();

        Flash::success('Detail Transaksi deleted successfully.');

        return redirect(route('detailTransaksis.index'));
    }
}
