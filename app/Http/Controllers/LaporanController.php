<?php

namespace App\Http\Controllers;

use App\Models\DetailTransaksi;
use App\Models\KeuanganKeluar;
use App\Models\Transaksi;
use Illuminate\Http\Request;

class LaporanController extends Controller
{
    public function index()
    {
        $months = [
            "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
        ];
        $graphLaporan = [];
        $laporanMonth = [];
        for ($i = 0; $i < count($months); $i++) {
            $this->month = $i + 1;
            $sumTransaksi = Transaksi::where('status', '=', 'selesai')->WhereYear('updated_at', date('Y'))->WhereMonth('updated_at', $this->month)->sum('grand_total');
            $itemsBuyPrice = 0;
            $detailTransaksi = DetailTransaksi::WhereYear('updated_at', date('Y'))->WhereMonth('updated_at', $this->month)->get();
            foreach ($detailTransaksi as $detailTransaksi => $value) {
                $itemsBuyPrice += $value->detailUkuranProduk->detailBeliProduk()->orderby('updated_at', 'desc')->first()->harga_beli * $value->jumlah;
            }
            $keuanganKeluar = KeuanganKeluar::where('validasi', 1)->WhereYear('updated_at', date('Y'))->WhereMonth('updated_at', $this->month)->sum('total');
            $pendapatan = ($sumTransaksi - $itemsBuyPrice) - $keuanganKeluar;
            if ($sumTransaksi != 0) {
                $dataLaporan = [
                    "bulan" => $months[$i],
                    "transaksi" => $sumTransaksi,
                    "pengeluaran" => $keuanganKeluar,
                    "pendapatan" => $pendapatan,
                ];
                array_push($laporanMonth, $dataLaporan);
            }
            array_push($graphLaporan, $pendapatan);
        }
        $graphLaporan = json_encode($graphLaporan, JSON_PRETTY_PRINT);
        return view('laporan.index', compact('graphLaporan', 'laporanMonth', 'months'));
    }
}
