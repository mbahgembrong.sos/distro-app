<div class="table-responsive">
    <table class="table" id="produks-table">
        <thead>
            <tr>
                <th>Kategori Id</th>
                <th>Nama</th>
                <th>Berat</th>
                <th>Deskripsi</th>
                <th>Foto</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($produks as $produk)
                <tr>
                    <td>{{ $produk->kategori->nama }}</td>
                    <td>{{ $produk->nama }}</td>
                    <td>{{ $produk->berat }}</td>
                    <td>{{ $produk->deskripsi }}</td>
                    <td> <img class="boxed--square--detail"
                                    src="{{ asset('storage/keuangan-keluar/' . $produk->foto) }}" />
                    </td>
                    @if (auth()->guard('karyawan')->user()->role()->first()->nama != 'Keuangan' &&
                        (auth()->guard('karyawan')->user()->role()->first()->nama != 'Pemilik' &&
                            $produk->validasi != 1))
                        <td width="120">
                            {!! Form::open(['route' => ['produks.destroy', $produk->id], 'method' => 'delete']) !!}
                            <div class='btn-group'>
                                <a href="{{ route('produks.show', [$produk->id]) }}" class='btn btn-default btn-xs'>
                                    <i class="far fa-eye"></i>
                                </a>
                                <a href="{{ route('produks.edit', [$produk->id]) }}" class='btn btn-default btn-xs'>
                                    <i class="far fa-edit"></i>
                                </a>
                                {!! Form::button('<i class="far fa-trash-alt"></i>', [
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'onclick' => "return confirm('Are you sure?')",
                                ]) !!}
                            </div>
                            {!! Form::close() !!}
                        </td>
                    @else
                        {{-- (auth()->guard('karyawan')->user()->role()->first()->nama == 'Pemilik' ) --}}
                        <td>
                            <a href="{{ route('produks.harga_jual', [$produk->id]) }}" class='btn btn-default btn-xs'>
                                <i class="far fa-eye"></i>
                            </a>
                        </td>
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
