@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Tambah Stock Produk</h1>
                </div>
                <div class="col-sm-6">
                    <a class="btn btn-default float-right" href="{{ route('produks.index') }}">
                        Back
                    </a>
                </div>
            </div>
        </div>
    </section>
    <div class="content px-3">
        <div class="card">
            {!! Form::open(['route' => 'produks.add_stock', 'files' => true]) !!}
            <div class="card-body">
                <div class="row">
                    @include('produks.show_fields')
                    <div class="form-group col-sm-12" id="transactions">
                        {!! Form::label('ukuran', 'Tambah Stock :') !!}
                        <div class="row">
                            @foreach ($produk->detailUkuranProduk as $item)
                                <div class="form-group fieldGroup col-12" data-id="1">
                                    <div class="input-group">
                                        <input type="hidden" name="detail_ukuran_produk_id[]" value="{{ $item->id }}">
                                        {!! Form::text('', $item->ukuran, ['class' => 'form-control', 'placeholder' => 'Ukuran', 'readonly']) !!}
                                        {!! Form::text('', $item->stock, ['class' => 'form-control', 'placeholder' => 'Stock', 'readonly']) !!}
                                        {!! Form::text('stock[]', null, ['class' => 'form-control', 'placeholder' => 'Jumlah']) !!}
                                        {!! Form::text('harga_beli[]', null, ['class' => 'form-control', 'placeholder' => 'Harga Beli']) !!}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <input type="hidden" name="produk_nama" value="{{ $produk->nama }}">
                    <div class="form-group col-sm-12">
                        {!! Form::label('foto', 'Foto bukti Pembelian:') !!}
                        <div class="input-group">
                            <div class="custom-file">
                                {!! Form::file('foto', ['class' => 'custom-file-input']) !!}
                                {!! Form::label('foto', 'Choose file', ['class' => 'custom-file-label']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                </div>
            </div>
            <div class="card-footer">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('produks.index') }}" class="btn btn-default">Cancel</a>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
    </div>
@endsection
