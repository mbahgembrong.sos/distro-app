<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Produk
 * @package App\Models
 * @version July 21, 2022, 2:01 pm UTC
 *
 * @property unsignedBigInteger $kategori_id
 * @property string $nama
 * @property integer $berat
 * @property integer $diskon
 * @property string $deskripsi
 * @property string $foto
 */
class Produk extends Model
{

    use HasFactory;

    public $table = 'produks';




    public $fillable = [
        'kategori_id',
        'nama',
        'berat',
        'deskripsi',
        'foto'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nama' => 'string',
        'berat' => 'integer',

        'deskripsi' => 'string',
        'foto' => 'string',
        'validasi' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'kategori_id' => 'required',
        'nama' => 'required|string',
        'berat' => 'required|integer',
        'deskripsi' => 'required',
        'foto' => 'required|file'
    ];
    /**
     * Get the kategori that owns the Produk
     *
     * @return \Illuminate\Database\Eloquent\Relations\
     */
    public function kategori()
    {
        return $this->belongsTo(Kategori::class, 'kategori_id', 'id');
    }
    /**
     * Get all of the detailUkuranProduk for the Produk
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function detailUkuranProduk()
    {
        return $this->hasMany(DetailUkuranProduk::class, 'produk_id', 'id');
    }
    /**
     * Get all of the detailBeliProduk for the Produk
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function detailBeliProduk()
    {
        return $this->hasMany(DetailBeliProduk::class, 'produk_id', 'id');
    }
    /**
     * Get all of the detailTransaksi for the Produk
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function detailTransaksi()
    {
        return $this->hasMany(DetailTransaksi::class, 'produk_id', 'id');
    }
    /**
     * Get all of the keranjang for the Produk
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function keranjang()
    {
        return $this->hasMany(Keranjang::class,'produk_id','id');
    }
}
