<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');




Route::resource('kategoris', App\Http\Controllers\KategoriController::class);


Route::resource('produks', App\Http\Controllers\ProdukController::class);
Route::prefix('produks')->group(function () {
    Route::post('/add_stock', [App\Http\Controllers\ProdukController::class, 'add_stock'])->name('produks.add_stock');
    Route::get('/harga_jual/{id}', [App\Http\Controllers\ProdukController::class, 'harga_jual'])->name('produks.harga_jual');
    Route::post('/harga_jual/{id}', [App\Http\Controllers\ProdukController::class, 'update_harga_jual'])->name('produks.harga_jual.update');
});

Route::resource('detailUkuranProduks', App\Http\Controllers\DetailUkuranProdukController::class);


Route::resource('roles', App\Http\Controllers\RoleController::class);


Route::resource('karyawans', App\Http\Controllers\KaryawanController::class);
Route::prefix('karyawan')->group(
    function () {
        Route::get('/login', [App\Http\Controllers\Auth\KaryawanAuthController::class, 'getLogin'])->name('karyawan.login');
        Route::post('/login', [App\Http\Controllers\Auth\KaryawanAuthController::class, 'postLogin'])->name('karyawan.post.login');
        Route::post('/logout', [App\Http\Controllers\Auth\KaryawanAuthController::class, 'postLogout'])->name('karyawan.logout');
    }
);

Route::resource('pelanggans', App\Http\Controllers\PelangganController::class);


Route::resource('tagihanans', App\Http\Controllers\TagihananController::class);


Route::resource('keuanganKeluars', App\Http\Controllers\KeuanganKeluarController::class);
Route::prefix('keuanganKeluars')->group(function () {
    Route::post('/validasi/{id}', [App\Http\Controllers\KeuanganKeluarController::class, 'validasi'])->name('keuanganKeluars.validasi');
});

Route::resource('transaksis', App\Http\Controllers\TransaksiController::class);


Route::resource('detailTransaksis', App\Http\Controllers\DetailTransaksiController::class);


Route::resource('detailPembayarans', App\Http\Controllers\DetailPembayaranController::class);


Route::resource('detailBeliProduks', App\Http\Controllers\DetailBeliProdukController::class);


Route::resource('keranjangs', App\Http\Controllers\KeranjangController::class);
Route::prefix('jurnal_harian')->group(function () {
    Route::get('/', [App\Http\Controllers\JurnalHarianController::class, 'index'])->name('jurnal_harian.index');
    Route::get('/keuangan_masuk', [App\Http\Controllers\JurnalHarianController::class, 'keuangan_masuk'])->name('jurnal_harian.keuangan_masuk');
    Route::post('/', [App\Http\Controllers\JurnalHarianController::class, 'store'])->name('jurnal_harian.store');
});
Route::prefix('laporan')->group(function () {
    Route::get('/', [App\Http\Controllers\LaporanController::class, 'index'])->name('laporan.index');
});
