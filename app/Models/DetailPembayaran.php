<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class DetailPembayaran
 * @package App\Models
 * @version July 21, 2022, 2:43 pm UTC
 *
 * @property unsignedBigInteger $transaksi_id
 * @property string $nama
 * @property string $bank
 * @property integer $jumlah
 * @property string $tanggal
 * @property string $bukti
 * @property boolean $validasi
 */
class DetailPembayaran extends Model
{

    use HasFactory;

    public $table = 'detail_pembayarans';




    public $fillable = [
        'transaksi_id',
        'nama',
        'bank',
        'jumlah',
        'tanggal',
        'bukti',
        'validasi'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nama' => 'string',
        'bank' => 'string',
        'jumlah' => 'integer',
        'tanggal' => 'date',
        'bukti' => 'string',
        'validasi' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'transaksi_id' => 'required',
        'nama' => 'required',
        'bank' => 'required',
        'jumlah' => 'required:integer',
        'tanggal' => 'required|date',
        'bukti' => 'required|file'
    ];
    /**
     * Get the transaksi that owns the DetailPembayaran
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function transaksi(): BelongsTo
    {
        return $this->belongsTo(Transaksi::class,'transaksi_id');
    }


}
